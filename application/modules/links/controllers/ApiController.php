<?php

class Links_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $updateDate = $this->getParam("updateDate");
        $type = $this->getParam("type");
        $category = $this->getParam("category");
        $parentId = $this->getParam("parentId");
        $deleted = $this->_getParam('deleted');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($type) && $filter["type"] = $type;
        isset($category) && $filter["category"] = $category;
        isset($parentId) && $filter["parentId"] = $parentId;
        isset($deleted) && $filter["deleted"] = $deleted;

        $dt = new DateTime($str);
        $dt->setTimeZone(new DateTimeZone('Asia/Tehran'));

        $links_service = Links_Service_Link::getInstance();
        $links = $links_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($links as $item) {
            $item = $item->toArray();
            if (isset($item['fileId'])) {
                $r_service = Resources_Service_Resource::getInstance();
                $file_id = $item['fileId'];
                $file_id && $file = $r_service->getByPK($file_id);
                if ($file) {
                    $config = Zend_Registry::get('config');
                    $file && $item['file'] = $file->toArray();
                    $item["path"] = $config->app->url . '/r/' . $file->getId();
                };
            }
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list,
            'updateDate' => $dt->format('Y-m-d H:i:s')
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Links_Service_Link::getInstance();
        $link = $service->getByPK($id);

        if ($link)
            $result = $link->toArray();
        else
            $result = array();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();

        $user = $authService->getCurrentUser();

        $link = new Links_Model_Link();
        $link->setupdateDate('now');
        $link->fill($params);
        $result = Links_Service_Link::getInstance()
                ->save($link);

        $item = $result->toArray();

        if (isset($item['fileId'])) {
            $r_service = Resources_Service_Resource::getInstance();
            $file_id = $item['fileId'];
            $file_id && $file = $r_service->getByPK($file_id);
            if ($file) {
                $config = Zend_Registry::get('config');
                $file && $item['file'] = $file->toArray();
                $item["path"] = $config->app->url . '/r/' . $file->getId();
            };
        }

        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($item));
            return;
        }
    }

    public function putAction()
    {
        $service = Links_Service_Link::getInstance();
        $params = $this->getParams();

        $link = $service->getByPK($params['id']);
        //TO Do
        if (!$link instanceof Links_Model_Link) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Link)');
            return;
        }

        $link->fill($params);
        $link->setUpdateDate('now');
        $link->setNew(false);
        $link = $service->save($link);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($link->toArray()));
    }

    public function deleteAction()
    {
        $service = Links_Service_Link::getInstance();
        $linkId = $this->_getParam('id');
        $link = $service->getByPK($linkId);

        if (!$link instanceof Links_Model_Link) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Link)");
            return;
        }

        $service->remove($link);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}


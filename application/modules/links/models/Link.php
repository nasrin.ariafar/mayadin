<?php

class Links_Model_Link extends Tea_Model_Entity
{

    const TYPE_RULES = 1;
    const TYPE_EDUCATION = 2;
//    -------------------------------
    const CATEGORY_OTHER = 0;
    const CATEGORY_FRUITAGE = 1;
    const CATEGORY_VEGETABLES = 2;
    const CATEGORY_PROTEIN = 3;
    const CATEGORY_AQUATIC = 4;
    
    const CATEGORY_HEALTH_ADVIC = 5;
//    const CATEGORY_SHORT_HEALTH_MESSAGES = 6;
//    const CATEGORY_DO_YOU_KNOW = 7;

    protected $_properties = array(
        'id' => NULL,
        'parentId' => 0,
        'title' => NULL,
        'description' => NULL,
        'path' => NULL,
        'type' => NULL,
        'category' => NULL,
        'isFile' => 1,
        'fileId' => NULL,
        'creationDate' => NULL,
        'updateDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'parentId' :
                case 'title' :
                case 'description' :
                case 'path' :
                case 'type' :
                case 'category' :
                case 'isFile' :
                case 'fileId' :
                case 'creationDate' :
                case 'updateDate' :
                case 'deleted':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}


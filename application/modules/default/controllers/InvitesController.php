<?php

class Default_InvitesController extends Tea_Controller_Action
{
    public function init()
    {
        $this->view->moduleLoader()->appendModule('invites');
        $this->view->headLink()->appendStylesheet('css/invites.css');
    }

    public function indexAction()
    {
        $session = new Zend_Session_Namespace('import_contacts');        
        $this->view->gmail_contacts = $session->gmail_contacts;
        $this->view->gmail_members  = $session->gmail_members;
        $this->view->yahoo_contacts = $session->yahoo_contacts;
        $this->view->yahoo_members  = $session->yahoo_members;
        $this->view->fb_contacts = $session->fb_contacts;
        $this->view->fb_members  = $session->fb_members;
        $this->view->twitter_contacts = $session->twitter_contacts;
        $this->view->twitter_members  = $session->twitter_members;
    }
    
    public function emailAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $config = Zend_Registry::get('config');
        $emails = $this->_getParam('emails');
        $cUser = Users_Service_Auth::getInstance()->getCurrentUser();
        $subject = 'Invitation to Memvoo.com From'.$cUser->getName();
        $type = $this->_getParam('type');
        $cUser  = Users_Service_Auth::getInstance()->getCurrentUser();
        $famSrv = Users_Service_FamiliarPeople::getInstance();

        if (!$cUser instanceof Users_Model_User) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (User)");
            return;
        }

        foreach ($emails as $email){
            
            
            if (isset($type)){
                if ($type == "gmail" || $type == "yahoo"){
                    $data   = array (
                        'template' => 'invitation',
                        'to'       => $email,
                        'subject'  => $this->_getParam('subject')
                    );
                    Tea_Service_Gearman::getInstance()->doBackground('default', 'email', $data);
                    
                    if ($type == "gmail"){
                        $type = Users_Model_FamiliarPeople::TYPE_GMAIL;
                    } else if ($type == "yahoo"){
                        $type = Users_Model_FamiliarPeople::TYPE_YAHOO;
                    }
                    
                    $filter = array(
                        'user_id' => $cUser->getId(),
                        'type' => $type,
                        'email' => $email
                    );
                    $result = "Success";
                    
                } else if ($type == "facebook"){
                    $type = Users_Model_FamiliarPeople::TYPE_FACEBOOK;
                    $filter = array(
                        'user_id' => $cUser->getId(),
                        'type' => $type,
                        'net_id' => $email
                    );
                    $result = "Success";
                    
                } else if ($type == "twitter"){
                    $session = new Zend_Session_Namespace('twitter_oauth');
                    $token = $session->token;
                    $twitter = new Zend_Service_Twitter(array(
                        'username' => $email,
                        'accessToken' => $token
                    ));
                    $result   = $twitter->directMessage->new('myfriend', $subject);
                    $result = $result->error();
                    
                    $type = Users_Model_FamiliarPeople::TYPE_TWITTER;
                    $filter = array(
                        'user_id' => $cUser->getId(),
                        'type' => $type,
                        'name' => $email
                    );
                }

                $familar = $famSrv->getByType($filter);
                $familar->setInvited(1);
                $famSrv->save($familar);
            }
        }
        $response = json_encode($result);
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody($response);
    }
    
    public function getContactsAction()
    {   
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        header('Access-Control-Allow-Origin: *');
        $type = $this->_getParam('type');
        
        $session = new Zend_Session_Namespace('import_contacts');
        if ($type == 'facebook') {
            $contacts = $session->fb_contacts;
            $members  = $session->fb_members;
            
        } elseif ($type == 'gmail') {
            $contacts = $session->gmail_contacts;
            $members  = $session->gmail_members;     
            $this->view->gmail_contacts = $session->gmail_contacts;
            $this->view->gmail_members  = $session->gmail_members;
            
        } elseif ($type == 'yahoo') {
            $contacts = $session->yahoo_contacts;
            $members  = $session->yahoo_members;
            
        } elseif ($type == 'twitter') {
            $contacts = $session->twitter_contacts;
            $members  = $session->twitter_members;   
        }
       
        $result = array(
            'members' => $members,
            'contacts' => $contacts
        );
       $response = json_encode($result);
       $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody($response);       
    }
}
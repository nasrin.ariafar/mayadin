<?php

class Users_AdminController extends Tea_Controller_Action
{

    public function init()
    {
        $this->view->moduleLoader()->appendModule('users-admin');
        $this->view->headLink()->appendStylesheet('css/users-admin.css');
    }

    public function indexAction()
    {
    }
}

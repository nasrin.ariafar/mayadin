<?php

class Users_Api_ServiceController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {

        $authService = Users_Service_Auth::getInstance();
        $user = $authService->getCurrentUser();

        $s_service = Users_Service_Service::getInstance();

        $userId = $this->_getParam('id', $user->getId());

        $service = $s_service->getByUserId($userId);

        if ($service) {
            if (strtotime(date('Y-m-d')) > strtotime($service->getExpireDate($count))) {
                $service->setFuzzyExpireDate(0);
            } else {
                $user_servic = Users_Service_User::getInstance();
                $fuzzy_date = $user_servic->getFuzzyDate($service->getExpireDate($count));
                $service->setFuzzyExpireDate($fuzzy_date);
            }
            $result = $service->toArray();
        }
        else
            $result = null;

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();

        $service = new Users_Model_Service();
        $service->fill($params);

        $result = Users_Service_Service::getInstance()
                ->save($service);

        if (!isset($result)) {
            $this->getResponse()
                    ->setHttpResponseCode(400)
                    ->appendBody("Error : " . $result);
            return;
        }

//        $userRegions_servic = Regions_Service_UserRegions::getInstance();
//        $userRegions_servic->addRegions($params['userId'], explode(',', $params['userRegions']));


        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($service->toArray()));
    }

    public function putAction()
    {
        $authService = Users_Service_Auth::getInstance();
        $user = $authService->getCurrentUser();
        $userRegions_servic = Regions_Service_UserRegions::getInstance();
        $params = $this->getParams();

        $service = Users_Service_Service::getInstance()->getByPK($params['id']);

        if ($params["type"] == "increaseBalanc") {
            $service->setBalanc($params['balanc']);
        } else {
            $service->fill($params);
            $service->setNew(false);
            $expire_date = $service->getExpireDate($count);

            if (!isset($expire_date) || isset($expire_date) && $expire_date <= date('Y-m-d H:i:s')) {
                $acount_duration = $params['acountDuration'];
                $date = $service->setexpireDate($params['acountDuration']); //Tea_Model_Entity::getExpireDate($params['acountDuration']);
                $service->setexpireDate(Date('Y-m-d', strtotime("+" . $acount_duration * 30 . " day") + 1));
                $service->setServiceType($params['serviceType']);
                $service->setAcountDuration($params['acountDuration']);

                $user_service = Users_Service_User::getInstance();

                $user->setStatus(Users_Model_User::STATUS_ACTIVE);
                $user->setServiceType($params['serviceType']);
                $user->setNew(false);
                $user->setFuzzyExpireDate($acount_duration * 30);
                $user_service->save($user);

                Zend_Auth::getInstance()->getStorage()->write($user);

                $userRegions_servic->remove($user->getId());
            }
            $userRegions_servic->addRegions($params['userId'], explode(',', $params['userRegions']));
        }
        $service->setNew(false);
        $s_service = Users_Service_Service::getInstance();
        $service = $s_service->save($service);
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($service->toArray()));
    }

}

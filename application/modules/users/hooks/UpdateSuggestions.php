<?php

class Users_Hook_UpdateSuggestions
{
    public function execute($event, $data)
    {
        switch ($event) {
        case 'follow_user':
        case 'accept_follow_user':
            if (!is_a($data, 'Users_Model_Follow')) {
                return false;
            }

            if ($data->getStatus() == Users_Model_Follow::STATUS_ACCEPT) {
                Tea_Service_Gearman::getInstance()->doBackground(
                    'users',
                    'update_suggestions',
                    $data
                );
            }
            break;
        }
    }
}

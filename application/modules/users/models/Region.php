<?php

class Users_Model_Region extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => null,
        'regionId' => null,
        'name' => null,
        'userId' => null,
        'creationDate' => null,
        'updateDate' => null,
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id':
                case 'regionId':
                case 'name':
                case 'userId':
                case 'creationDate':
                case 'updateDate':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

    public static function hasProperty($property)
    {
        switch ($property) {
            case 'id':
            case 'regionId':
            case 'name':
            case 'userId':
            case 'creationDate':
            case 'updateDate':
                return true;
        }

        return false;
    }

}

<?php

class Users_Validate_ExistProfile extends Zend_Validate_Abstract
{
    const NOT_EXIST = 'notExist';
    
    protected $_messageTemplates = array(
        self::NOT_EXIST => "No profile was found by this id",
    );
    
    public function isValid($value)
    {
        $profileService = Users_Service_Profile::getInstance();
        
        $profile = $profileService->getByPK($value);
        if (!$profile instanceof Users_Model_Profile) {
            $this->_error(self::NOT_EXIST);
            return false;
        }
        
        return true;
    }
}

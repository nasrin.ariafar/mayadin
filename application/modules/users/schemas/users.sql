SET FOREIGN_KEY_CHECKS=0;
--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
    `id`                BIGINT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `first_name`        VARCHAR(64)     NULL,
    `middle_name`       VARCHAR(64),
    `last_name`         VARCHAR(64)     NULL,
    `email`             VARCHAR(128)    NULL,
    `username`          VARCHAR(128)    NULL,
    `password`          VARCHAR(64)     NULL,
    `gender`            ENUM('FEMALE', 'MALE', 'UNSPECIFIED')  NULL default 'UNSPECIFIED',
    `photo_id`          BIGINT(20)      UNSIGNED,
    `birth_date`        DATE            NULL,
    `timezone`          VARCHAR(32)     NULL,
    `locale`            VARCHAR(16)     NULL,
    `profile_id`        BIGINT(20)      UNSIGNED,
    `status`            ENUM('WAITING','ACTIVE','INACTIVE','BLOCKED') DEFAULT 'WAITING',
    `join_date`         DATETIME        NULL,
    `created_by_id`     BIGINT(20)      UNSIGNED  NULL,
    `creation_date`     DATETIME        NULL,
    `updated_by_id`     BIGINT(20)      UNSIGNED   NULL,
    `update_date`       DATETIME        NULL,

    PRIMARY KEY (`id`)

) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


--
-- Table structure for table `users_profiles`
--

DROP TABLE IF EXISTS `user_profiles`;
CREATE TABLE IF NOT EXISTS `user_profiles` (
    `id`                BIGINT(20)  UNSIGNED NOT NULL AUTO_INCREMENT,
    `first_name_e`      VARCHAR(64),
    `middle_name_e`     VARCHAR(64),
    `last_name_e`       VARCHAR(64),
    `father_name`       VARCHAR(64),
    `birth_loc`         VARCHAR(64),
    `identity_no`       VARCHAR(64),
    `identity_date`     DATE,
    `identity_loc`      VARCHAR(64),
    `cell`              VARCHAR(16),
    `cell2`             VARCHAR(16),
    `phone`             VARCHAR(16),
    `country`           VARCHAR(64),
    `state`             VARCHAR(64),
    `city`              VARCHAR(64),
    `street`            VARCHAR(64),
    `zip_code`          VARCHAR(64),
    `home_phone`        VARCHAR(16),
    `home_country`      VARCHAR(64),
    `home_state`        VARCHAR(64),
    `home_city`         VARCHAR(64),
    `home_street`       VARCHAR(64),
    `home_zip_code`     VARCHAR(64),
    `marital`           TINYINT(1),
    `military`          VARCHAR(64),
    `military_loc`      VARCHAR(64),
    `nationality`       VARCHAR(64),
    `religion`          VARCHAR(64),
    `about`             VARCHAR(256),
    `created_by_id`     BIGINT(20)    UNSIGNED NOT NULL,
    `creation_date`     DATETIME        NOT NULL,
    `updated_by_id`     BIGINT(20)    UNSIGNED NOT NULL,
    `update_date`       DATETIME        NOT NULL,

    PRIMARY KEY (`id`)

) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


--
-- Table structure for table `users_prefs`
--

DROP TABLE IF EXISTS `user_prefs`;
CREATE TABLE IF NOT EXISTS `user_prefs` (
    `user_id`       BIGINT(20)    UNSIGNED  NOT NULL,
    `key`           VARCHAR(64)   NOT NULL,
    `value`         VARCHAR(256)  NULL,
    `creation_date` DATETIME      NOT NULL,
    `update_date`   DATETIME      NOT NULL,

    PRIMARY KEY (`user_id`, `key`)

) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;



--
-- Table structure for table `users_networks`
--

DROP TABLE IF EXISTS `user_networks`;
CREATE TABLE IF NOT EXISTS `user_networks` (
    `id`                  BIGINT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id`             BIGINT(20)      UNSIGNED NOT NULL,
    `username`            VARCHAR(128)    NULL,
    `oauth_id`            VARCHAR(128)    NOT NULL,
    `provider`            VARCHAR(256)    NOT NULL,
    `access_token`        VARCHAR(256)    NOT NULL,
    `access_token_secret` VARCHAR(256)    NULL,
    `status`              TINYINT(1)      NULL,   -- 1:Active, 2:Inactvie
    `created_by_id`       BIGINT(20)      UNSIGNED NULL,
    `creation_date`       DATETIME        NULL,
    `updated_by_id`       BIGINT(20)      UNSIGNED NULL,
    `update_date`         DATETIME        NULL,

    PRIMARY KEY (`id`)

) ENGINE=InnoDB  AUTO_INCREMENT=1 ;



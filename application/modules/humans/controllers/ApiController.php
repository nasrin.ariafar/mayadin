<?php

class Humans_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $filter = array();
        $updateDate = $this->_getParam('updateDate');
        $type = $this->_getParam('type');
        $deleted = $this->_getParam('deleted');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($type) && $filter["type"] = $type;
        isset($deleted) && $filter["deleted"] = $deleted;

        $humans_service = Humans_Service_Human::getInstance();
        $humans = $humans_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($humans as $item) {
            $item = $item->toArray();
            $r_service = Resources_Service_Resource::getInstance();
            $images = $r_service->getImagesByParent(array(
                "parentId" => $item["id"],
                "parentType" => "human"
                    ));
            $item["photos"] = $images;
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Humans_Service_Human::getInstance();
        $human = $service->getByPK($id);

        if ($human)
            $result = $human->toArray();
        else
            $result = array();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
//        $authService = Humans_Service_Auth::getInstance();
//        $user = $authService->getCurrentUser();
//        if (!isset($user)) {
//            $this->getResponse()
//                    ->setHttpResponseCode(404)
//                    ->appendBody('Unauthorized');
//            return;
//        }

        $human = new Humans_Model_Human();
        $human->setupdateDate('now');
        $human->fill($params);
        $result = Humans_Service_Human::getInstance()
                ->save($human);
        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($result->toArray()));
            return;
        }
    }

    public function putAction()
    {
        $service = Humans_Service_Human::getInstance();
        $params = $this->getParams();

        $human = $service->getByPK($params['id']);
        //TO Do
        if (!$human instanceof Humans_Model_Human) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Human)');
            return;
        }

        $human->fill($params);
        $human->setUpdateDate('now');
        $human->setNew(false);
        $human = $service->save($human);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($human->toArray()));
    }

    public function deleteAction()
    {
        $service = Humans_Service_Human::getInstance();
        $humanId = $this->_getParam('id');
        $human = $service->getByPK($humanId);

        if (!$human instanceof Humans_Model_Human) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Human)");
            return;
        }

        $service->remove($human);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}


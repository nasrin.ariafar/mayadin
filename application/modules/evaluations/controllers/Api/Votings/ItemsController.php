<?php

class Polls_Api_Votings_ItemsController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function postAction()
    {
        $pollService        = Polls_Service_Poll::getInstance();
        $votingItemService  = Polls_Service_VotingItem::getInstance();

        if ($this->_currentUser == null) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (User)");
            return;
        }
        
        $pollId = $this->_getParam('id');
        $poll   = $pollService->getByPK($pollId);
        if (!$poll instanceof Polls_Model_Poll) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Poll)");
            return;
        }
        
        $votingId = $this->_getParam('sub_id');
        
        $voting  = null;
        $votings = $poll->getQuestions();
        foreach ($votings as $_voting) {
            if ($_voting->getId() == $votingId) {
                $voting = $_voting;
                break;
            }
        }
        
        if ($voting == null) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Voting)");
            return;
        }
        
        $params = $this->getParams();
        unset($params['id']);
        unset($params['sub_id']);
        
        $valid = $this->checkParams(array(
            'text',
            'value'
        ));
            
        $item = new Polls_Model_VotingItem();
        $item->fill($params);
        $item->setPollId($poll->getId());
        $item->setVotingId($voting->getId());
        $item->setCreatedById($this->_currentUser->getId());
        $item->setUpdatedById($this->_currentUser->getId());
        $item->setNew(true);
        
        $item = $votingItemService->save($item);

        $_item = $item->toArray();
        $response = json_encode($_item);
        $this->getResponse()
            ->setHttpResponseCode(200)
            ->appendBody($response);
    }

    public function deleteAction()
    {
        $pollService        = Polls_Service_Poll::getInstance();
        $votingItemService  = Polls_Service_VotingItem::getInstance();

        $pollId = $this->_getParam('id');
        $poll   = $pollService->getByPK($pollId);
        if (!$poll instanceof Polls_Model_Poll) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Poll)");
            return;
        }
        
        $votingId = $this->_getParam('sub_id');
        $itemId   = $this->_getParam('sub2_id');
        
        $voting = null;
        $item   = null;
        $votings = $poll->getQuestions();
        foreach ($votings as $_voting) {
            if ($_voting->getId() == $votingId) {
                $voting = $_voting;
                
                $items = $voting->getItems();
                foreach ($items as $_item) {
                    if ($_item->getId() == $itemId) {
                        $item = $_item;
                    }
                }
                
                break;
            }
        }
        
        if ($voting == null) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Voting)");
            return;
        }
        
        if ($item == null) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (VotingItem)");
            return;
        }

        $votingItemService->remove($item);

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->appendBody("Success");
    }

}

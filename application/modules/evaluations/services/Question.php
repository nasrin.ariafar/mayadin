<?php

class Evaluations_Service_Question extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Evaluations_Model_DbTable_Questions();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Evaluations_Model_Question $question = null)
    {
        $questionItemService = Evaluations_Service_QuestionItem::getInstance();

        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$question instanceof Evaluations_Model_Question) {
            $question = new Evaluations_Model_Question();
        }
        $question->setNew(false);
        $question->fill($row);


        // ------------------ Retrieve QuestionItems -------------------
        $filter = array(
            'questionId' => $question->getId()
        );

//        $sort = array(
//            'ordering' => 'asc'
//        );
//
        $items = $questionItemService->getList($filter, $sort, 0, $iCount, 1000);

        $question->setItems($items);
        return $question;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $questionItemService = Evaluations_Service_QuestionItem::getInstance();
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $cSelect->from(array('v' => 'questions'), array('COUNT(*) AS count'));

        $select->from(array('v' => 'questions'));

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                    default:
                        $key = "v.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                }
            }
        }

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "v.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $questions = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {

            $question = new Evaluations_Model_Question();
            $question->fill($row);
            $question->setNew(false);

            // ------------------ Retrieve Questions -------------------
            $filter = array(
                'questionId' => $question->getId()
            );

            $sort = array(
                'ordering' => 'asc'
            );

            $items = $questionItemService->getList($filter, $sort, 0, $iCount, 1000);

            $question->setItems($items);

            $questions[] = $question;
        }

        return $questions;
    }

    public function save(Evaluations_Model_Question $question, $triggerEvent = true)
    {
        if ($question->isNew()) {
            $data = $question->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                $question = $this->getByPK($pk, $question);
                if ($triggerEvent) {
                    $evaluation = Evaluations_Service_Evaluation::getInstance()->getByPK($question->getEvaluationId());
//                    Tea_Hook_Registry::dispatchEvent('edit_poll', $evaluation);
                }
                return $question;
            }
        } else {
            $id = $question->getId();
            $data = $question->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);

            $question = $this->getByPK($id, $question);
            if ($triggerEvent) {
                $evaluation = Evaluations_Service_Evaluation::getInstance()->getByPK($question->getEvaluationId());
//                Tea_Hook_Registry::dispatchEvent('edit_poll', $evaluation);
            }
            return $question;
        }

        return false;
    }

    public function saveCompleteQuestion(Evaluations_Model_Question $question)
    {
        $questionItemService = Evaluations_Service_QuestionItem::getInstance();

        $items = $question->getItems();

        $this->_table->getAdapter()->beginTransaction();
        try {
            $question = $this->save($question, false);

            if (is_array($items)) {
                foreach ($items as $item) {
                    $item->setEvaluationId($question->getEvaluationId());
                    $item->setQuestionId($question->getId());
                    $item = $questionItemService->save($item, false);
                }
            }

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }

        $question = $this->getByPK($question->getId());
//        $evaluation   = Evaluations_Service_Evaluation::getInstance()->getByPK($question->getEvaluationId());
//                      Tea_Hook_Registry::dispatchEvent('edit_poll', $evaluation);

        return $question;
    }

    public function remove(Evaluations_Model_Question $question, $triggerEvent = true)
    {
        $questionItemService = Evaluations_Service_QuestionItem::getInstance();
        $questionResponseService = Evaluations_Service_Response::getInstance();

        $questionItemService->removeByQuestion($question);
        $questionResponseService->removeByQuestion($question);
        //----------------------------------------------------------

        $where = $this->_table->getAdapter()->quoteInto('id = ?', $question->getId());
        $this->_table->delete($where);
    }

    public function removeByEvaluation(Evaluations_Model_Evaluation $evaluation)
    {
        $questionItemService = Evaluations_Service_QuestionItem::getInstance();
        $questionResponseService = Evaluations_Service_Response::getInstance();

        $this->_table->getAdapter()->beginTransaction();
        try {
            //------------- delete related tables entries ---------------
            $questionResponseService->removeByEvaluation($evaluation);
            $questionItemService->removeByEvaluation($evaluation);
            //-----------------------------------------------------------

            $where = $this->_table->getAdapter()->quoteInto('poll_id = ?', $evaluation->getId());
            $this->_table->delete($where);

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }
    }

    public function removeAll()
    {
        $questionItemService = Evaluations_Service_QuestionItem::getInstance();
        $questionResponseService = Evaluations_Service_Response::getInstance();

        $this->_table->getAdapter()->beginTransaction();
        try {
            //------------- delete related tables entries ---------------
            $questionItemService->removeAll();
            $questionResponseService->removeAll();
            //-----------------------------------------------------------

            $this->_table->delete('');

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }
    }

}

?>

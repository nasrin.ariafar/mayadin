<?php

class Evaluations_Service_Response extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Evaluations_Model_DbTable_Responses();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($responderId, $questionId, $value, Evaluations_Model_Response $questionResponse = null)
    {
        $rows = $this->_table->find($responderId, $questionId, $value);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();

        if (!$questionResponse instanceof Evaluations_Model_Response) {
            $questionResponse = new Evaluations_Model_Response();
        }
        $questionResponse->fill($row);
        $questionResponse->setNew(false);
        return $questionResponse;
    }

    public function getByResponderId($responderId, $evaluationId, Evaluations_Model_Responsder $response = null)
    {
        $cSelect = $this->_table->select();
        $cSelect->where("evaluationId = ?", $evaluationId);
        $cSelect->where("responderId = ?", $responderId);
        $cSelect->from($this->_table, array('COUNT(DISTINCT evaluationId, responderId) AS count'));

        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        return $count;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->select();
        $select->where("evaluationId = ?", $avaluation->getId());

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                $select->where("{$key} = ?", $value);
            }
        }

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $select->order($key . ' ' . strtoupper($ord));
            }
        }
        $cSelect = clone $select;
        $cSelect->from($this->_table, array('COUNT(*) AS count'));
        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $result = array();
        foreach ($this->_table->fetchAll($select) as $row) {
            $questionResponse = new Evaluations_Model_Response();
            $questionResponse->fill($row);
            $questionResponse->setNew(false);
            $result[] = $questionResponse;
        }

        return $result;
    }

    public function countEvaluationResponses(Evaluations_Model_Evaluation $avaluation)
    {
        $cSelect = $this->_table->select();
        $cSelect->where("evaluationId = ?", $avaluation->getId());
        $cSelect->from($this->_table, array('COUNT(DISTINCT evaluationId, responderId) AS count'));

        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        return $count;
    }

    public function countQuestionResponses(Evaluations_Model_Question $question)
    {
        $cSelect = $this->_table->select();
        $cSelect->where("questionId = ?", $question->getId());
        $cSelect->from($this->_table, array('COUNT(DISTINCT questionId, responderId) AS count'));

        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        return $count;
    }

    public function countItemResponses(Evaluations_Model_QuestionItem $item)
    {
        $cSelect = $this->_table->select();
        $cSelect->where("questionId = ?", $item->getQuestionId());
        $cSelect->where("value = ?", $item->getValue());
        $cSelect->from($this->_table, array('COUNT(DISTINCT responderId) AS count'));

        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        return $count;
    }

    public function save(Evaluations_Model_Response $questionResponse)
    {
        if ($questionResponse->isNew()) {
            $data = $questionResponse->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk['responderId'], $pk['questionId'], $pk['value'], $questionResponse);
            }
        } else {
            $data = $questionResponse->toArray(true);
            $where1 = $this->_table->getAdapter()->quoteInto('responderId= ?', $questionResponse->getResponderId());
            $where2 = $this->_table->getAdapter()->quoteInto('questionId = ?', $questionResponse->getQuestionId());
            $where3 = $this->_table->getAdapter()->quoteInto('value = ?', $questionResponse->getValue());
            $where = $where1 . " AND " . $where2 . " AND " . $where3;
            $this->_table->update($data, $where);
            return $this->getByPK($questionResponse->getResponderId(), $questionResponse->getQuestionId(), $questionResponse->getValue(), $questionResponse);
        }
        return false;
    }

    public function remove(Evaluations_Model_Response $questionResponse)
    {
        $where1 = $this->_table->getAdapter()->quoteInto('responderId= ?', $questionResponse->getResponderId());
        $where2 = $this->_table->getAdapter()->quoteInto('questionId = ?', $questionResponse->getQuestionId());
        $where3 = $this->_table->getAdapter()->quoteInto('value = ?', $questionResponse->getValue());
        $where = $where1 . " AND " . $where2 . " AND " . $where3;
        $this->_table->delete($where);
    }

    public function removeByEvaluation(Evaluations_Model_Evaluation $avaluation)
    {
        $where = $this->_table->getAdapter()->quoteInto('evaluationId = ?', $avaluation->getId());
        $this->_table->delete($where);
    }

    public function removeByQuestion(Evaluations_Model_Question $question)
    {
        $where = $this->_table->getAdapter()->quoteInto('questionId = ?', $question->getId());
        $this->_table->delete($where);
    }

    public function removeByItem(Evaluations_Model_QuestionItem $item)
    {
        $where1 = $this->_table->getAdapter()->quoteInto('evaluationId = ?', $item->getEvaluationId());
        $where2 = $this->_table->getAdapter()->quoteInto('questionId = ?', $item->getQuestionId());
        $where3 = $this->_table->getAdapter()->quoteInto('value = ?', $item->getValue());
        $where = "$where1 AND $where2 AND $where3";
        $this->_table->delete($where);
    }

    public function removeUserResponsesOfEvaluation(Users_Model_User $user, Evaluations_Model_Evaluation $avaluation)
    {
        $where1 = $this->_table->getAdapter()->quoteInto('responderId= ?', $user->getId());
        $where2 = $this->_table->getAdapter()->quoteInto('evaluationId = ?', $avaluation->getId());
        $where = $where1 . " AND " . $where2;
        $this->_table->delete($where);

        Tea_Hook_Registry::dispatchEvent('delete_response', $avaluation);
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

}

?>

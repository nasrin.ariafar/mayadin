<?php

class Evaluations_Model_QuestionItem extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => null,
        'evaluationId' => null,
        'questionId' => null,
        'text' => null,
        'value' => null,
        'ordering' => null,
        'responseCount' => null,
        'statistics' => null,
        'deleted' => 0,
    );

    public function __construct()
    {
        parent::__construct();

        $this->setResponseCount(0);
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'ordering':
                case 'evaluationId' :
                case 'questionId' :
                case 'text' :
                case 'value' :
                case 'responseCount' :
                case 'statistics' :
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}

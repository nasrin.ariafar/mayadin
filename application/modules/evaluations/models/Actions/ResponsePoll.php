<?php

class Polls_Model_Actions_ResponsePoll extends Tea_Model_Actions_Abstract
{
    public function getNotificationMessage($activity)
    {
        $actor = $activity->getActor();
        if (!$actor instanceof Users_Model_User) {
            return false;
        }

        return $actor->getFirstName() . ' ' . $actor->getLastname() . ' created a response to your poll';
    }

    public function getFollowers($activity)
    {
        $userFollowService  = Users_Service_Follow::getInstance();
        
        $followersOfUser = $userFollowService->getUserFollowersIds($activity->getActorId());
        
        $uids = array(
            $activity->getActorId()
        );
        $uids = array_merge($uids, $followersOfUser);

        return array_unique($uids);
    }

    public function getNotifReceivers($activity)
    {
        $pollService = Polls_Service_Poll::getInstance();
        
        $pollId = $activity->getRefId1();
        $poll   = $pollService->getByPK($pollId);
        if (!$poll instanceof Polls_Model_Poll) {
            return;
        }
        
        $uids = array(
            $poll->getCreatedById()
        );

        $uids = array_unique($uids);
        if (array_search($activity->getActorId(), $uids) !== false) {
            unset($uids[$activity->getActorId()]);
        }
        
        return $uids;
    }
}

<?php

class Priceheads_Model_Pricehead extends Tea_Model_Entity
{

      protected $_properties = array(
        'productPriceNumber' => NULL,
        'efficacyDate' => NULL,
        'description' => NULL,
        'updateDate' => NULL,
        'creationDate' => NULL,
        'deleted' => 0
    );
      

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'productPriceNumber' :
                case 'efficacyDate' :
                case 'description':
                case 'updateDate' :
                case 'creationDate' :
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}


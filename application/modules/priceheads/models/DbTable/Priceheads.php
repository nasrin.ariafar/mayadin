<?php

class Priceheads_Model_DbTable_Priceheads extends Zend_Db_Table_Abstract
{
    protected $_name    = 'product_price_head';
    protected $_primary = 'productPriceNumber';
}

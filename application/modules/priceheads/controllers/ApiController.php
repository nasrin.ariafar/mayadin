<?php

class Priceheads_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $updateDate = $this->getParam("updateDate");
        $type = $this->getParam("type");
        $category = $this->getParam("category");
        $parentId = $this->getParam("parentId");
        $deleted = $this->_getParam('deleted');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($type) && $filter["type"] = $type;
        isset($category) && $filter["category"] = $category;
        isset($parentId) && $filter["parentId"] = $parentId;
        isset($deleted) && $filter["deleted"] = $deleted;

        $dt = new DateTime($str);
        $dt->setTimeZone(new DateTimeZone('Asia/Tehran'));

        $priceheads_service = Priceheads_Service_Pricehead::getInstance();
        $priceheads = $priceheads_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($priceheads as $item) {
            $item = $item->toArray();
            if (isset($item['fileId'])) {
                $r_service = Resources_Service_Resource::getInstance();
                $file_id = $item['fileId'];
                $file_id && $file = $r_service->getByPK($file_id);
                if ($file) {
                    $config = Zend_Registry::get('config');
                    $file && $item['file'] = $file->toArray();
                    $item["path"] = $config->app->url . '/r/' . $file->getId();
                };
            }
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list,
            'updateDate' => $dt->format('Y-m-d H:i:s')
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Priceheads_Service_Pricehead::getInstance();
        $pricehead = $service->getByPK($id);

        if ($pricehead)
            $result = $pricehead->toArray();
        else
            $result = array();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();

        $user = $authService->getCurrentUser();

        $pricehead = new Priceheads_Model_Pricehead();
        $pricehead->setupdateDate('now');
        $pricehead->fill($params);
        $result = Priceheads_Service_Pricehead::getInstance()
                ->save($pricehead);

        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode( $result->toArray()));
            return;
        }
    }

    public function putAction()
    {
        $service = Priceheads_Service_Pricehead::getInstance();
        $params = $this->getParams();

        $pricehead = $service->getByPK($params['id']);
        //TO Do
        if (!$pricehead instanceof Priceheads_Model_Pricehead) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Pricehead)');
            return;
        }

        $pricehead->fill($params);
        $pricehead->setUpdateDate('now');
        $pricehead->setNew(false);
        $pricehead = $service->save($pricehead);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($pricehead->toArray()));
    }

    public function deleteAction()
    {
        $service = Priceheads_Service_Pricehead::getInstance();
        $priceheadId = $this->_getParam('id');
        $pricehead = $service->getByPK($priceheadId);

        if (!$pricehead instanceof Priceheads_Model_Pricehead) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Pricehead)");
            return;
        }

        $service->remove($pricehead);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}


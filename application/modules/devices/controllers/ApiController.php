<?php

class Devices_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        
    }

    public function getAction()
    {

    }

    public function postAction()
    {
        $params = $this->getParams();
        $device = new Devices_Model_Device();
        $device->fill($params);

        $result = Devices_Service_Device::getInstance()
                ->save($device);
        
        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody("Success");
        }
        else {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody("Error");
        }

        return;
    }

    public function putAction()
    {

    }

    public function deleteAction()
    {
        
    }

}

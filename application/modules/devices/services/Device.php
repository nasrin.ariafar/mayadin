<?php

class Devices_Service_Device extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Devices_Model_DbTable_Devices();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByDeviceId($deviceId)
    {
        if (empty($deviceId)) {
            return false;
        }

        $rows = $this->_table->fetchAll(
                $this->_table->select()->where('deviceId = ?', $deviceId)
        );

        if (count($rows) == 0) {
            return false;
        }

        $device = new Devices_Model_Device;
        $device->fill($rows[0]);
        $device->setNew(false);

        return $device;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->select();

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                $select->where("{$key} = ?", $value);
            }
        }

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        $cSelect = clone $select;
        $cSelect->from($this->_table, array('COUNT(*) AS count'));

        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $result = array();
        foreach ($this->_table->fetchAll($select) as $row) {
            $objectTag = new Tags_Model_ObjectTag();
            $objectTag->fill($row);
            $objectTag->setNew(false);
            $result[] = $objectTag;
        }

        return $result;
    }

    public function search($query, $limit = 10)
    {
        $select = $this->_table->select()
                ->where("tag LIKE '%$query%'")
                ->group('tag')
                ->limit($limit);

        $result = array();
        foreach ($this->_table->fetchAll($select) as $row) {
            $result[] = $row['tag'];
        }

        return $result;
    }

    public function save(Devices_Model_Device $device)
    {
        if ($device->isNew()) {
            $deviceId = $this->getByDeviceId($device->getDeviceId());
            if ($deviceId) {
                $device->setNew(false);
            } else {
                $data = $device->toArray(true);
                $pk = $this->_table->insert($data);
            }
            return true;
        }
        return false;
    }

}

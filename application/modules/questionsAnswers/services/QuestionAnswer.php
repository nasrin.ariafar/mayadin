<?php

class QuestionsAnswers_Service_QuestionAnswer extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new QuestionsAnswers_Model_DbTable_QuestionsAnswers();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, QuestionsAnswers_Model_QuestionAnswer $questionAnswer = null)
    {
        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$questionAnswer instanceof QuestionsAnswers_Model_QuestionAnswer) {
            $questionAnswer = new QuestionsAnswers_Model_QuestionAnswer();
        }
        $questionAnswer->fill($row);
        $questionAnswer->setNew(false);
        return $questionAnswer;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $select->from(array('u' => 'questions_answers'), array('*'));
        $cSelect->from(array('u' => 'questions_answers'), array('COUNT(*) AS count'));

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "u.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                    case 'updateDate':
                        $select->where("u.updateDate > '$value'");
                        $cSelect->where("u.updateDate > '$value'");
                        break;

                    case "has_answer" :
                        if ($value == "false") {
                            $select->where("answer IS NULL");
                            $cSelect->where("answer IS NULL ");
                        } else {
                            $select->where("answer >", "");
                            $cSelect->where("answer >", "");
                        }
                        break;

                    
                    default:
                        $key = "u.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                        break;
                }
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $questionsAnswers = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $item = new QuestionsAnswers_Model_QuestionAnswer();
            $item->fill($row);
            $questionsAnswers[] = $item;
        }

        return $questionsAnswers;
    }

    public function save(QuestionsAnswers_Model_QuestionAnswer $questionAnswer)
    {
        if ($questionAnswer->isNew()) {
            $data = $questionAnswer->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $questionAnswer);
            }
        } else {
            $id = $questionAnswer->getId();
            $data = $questionAnswer->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $questionAnswer);
        }

        return false;
    }

    public function remove(QuestionsAnswers_Model_QuestionAnswer $questionAnswer)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $questionAnswer->getId());
        $this->_table->delete($where);
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

}

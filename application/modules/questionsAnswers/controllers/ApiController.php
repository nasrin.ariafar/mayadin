<?php

class QuestionsAnswers_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit');
        $start = $this->_getParam('start', 0);

        $updateDate = $this->getParam("updateDate");
        $deleted = $this->_getParam('deleted');
        $has_answer = $this->_getParam('has_answer');
        $type = $this->_getParam('type');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($deleted) && $filter["deleted"] = $deleted;
        isset($type) && $filter["type"] = $type;
        isset($has_answer) && $filter["has_answer"] = $has_answer;

        $dt = new DateTime($str);
        $dt->setTimeZone(new DateTimeZone('Asia/Tehran'));

        $questionAnswer_service = QuestionsAnswers_Service_QuestionAnswer::getInstance();
        $questionsAnswers = $questionAnswer_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($questionsAnswers as $item) {
            $item = $item->toArray();
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list,
            'updateDate' => $dt->format('Y-m-d H:i:s')
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = QuestionsAnswers_Service_QuestionAnswer::getInstance();
        $questionAnswer = $service->getByPK($id);

        if (!$questionAnswer) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (QuestionAnswer)");
            return;
        }

        $result = $questionAnswer->toArray();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();
        $user = $authService->getCurrentUser();

        $questionAnswer = new QuestionsAnswers_Model_QuestionAnswer();
        $questionAnswer->setCreationDate('now');
        $questionAnswer->setupdateDate('now');
        $questionAnswer->fill($params);

        $result = QuestionsAnswers_Service_QuestionAnswer::getInstance()
                ->save($questionAnswer);
        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($result->toArray()));
            return;
        }
    }

    public function putAction()
    {
        $service = QuestionsAnswers_Service_QuestionAnswer::getInstance();
        $params = $this->getParams();

        $questionAnswer = $service->getByPK($params['id']);
        //TO Do
        if (!$questionAnswer instanceof QuestionsAnswers_Model_QuestionAnswer) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (QuestionAnswer)');
            return;
        }

        $questionAnswer->fill($params);
        $questionAnswer->setUpdateDate('now');
        $questionAnswer->setNew(false);
        $questionAnswer = $service->save($questionAnswer);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($questionAnswer->toArray()));
    }

    public function deleteAction()
    {
        $service = QuestionsAnswers_Service_QuestionAnswer::getInstance();
        $questionAnswerId = $this->_getParam('id');
        $questionAnswer = $service->getByPK($questionAnswerId);

        if (!$questionAnswer instanceof QuestionsAnswers_Model_QuestionAnswer) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (QuestionAnswer)");
            return;
        }

        $service->remove($questionAnswer);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}


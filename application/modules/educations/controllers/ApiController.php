<?php

class Educations_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);
        $category = $this->getParam("category");

        $updateDate = $this->getParam("updateDate");
        $deleted = $this->_getParam('deleted');

        $filter = array();
        isset($deleted) && $filter["deleted"] = $deleted;
        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($category) && $filter["category"] = $category;

        $education_service = Educations_Service_Education::getInstance();
        $educations = $education_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($educations as $item) {
            $item = $item->toArray();
            $r_service = Resources_Service_Resource::getInstance();
            $imageFilter = array(
                "parentId" => $item["id"],
                "parentType" => "educations"
            );
            isset($deleted) && $imageFilter["deleted"] = $deleted;
            $images = $r_service->getImagesByParent($imageFilter);
            $item["photos"] = $images;
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Educations_Service_Education::getInstance();
        $education = $service->getByPK($id);
        $deletedImage = $this->_getParam('deletedImage');

        if (!$education) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Education)");
            return;
        }

        $result = $education->toArray();
        $r_service = Resources_Service_Resource::getInstance();
        $imageFilter = array(
            "parentId" => $result["id"],
            "parentType" => "educations"
        );
        isset($deletedImage) && $imageFilter["deleted"] = $deletedImage;
        $images = $r_service->getImagesByParent($imageFilter);
        $result["photos"] = $images;

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();
        $user = $authService->getCurrentUser();
        $images = $params["images"];
//        if (!isset($user)) {
//            $this->getResponse()
//                    ->setHttpResponseCode(404)
//                    ->appendBody('Unauthorized');
//            return;
//        }

        $edcation = new Educations_Model_Education();
        $edcation->setCreationDate('now');
        $edcation->setupdateDate('now');
        $edcation->fill($params);

        $result = Educations_Service_Education::getInstance()
                ->save($edcation);

        if ($result) {
            $new_item = $result->toArray();

            if (count($images)) {
                foreach ($images as $img) {
                    $r_model = new Resources_Model_Resource();
                    $r_service = Resources_Service_Resource::getInstance();

                    $img['parentId'] = $result->getId();
                    $r_model->fill($img);
                    $r_model->setNew(false);
                    $r_service->save($r_model);
                }
                $new_item["photos"] = $images;
            }

            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($new_item));
            return;
        }
    }

    public function putAction()
    {
        $service = Educations_Service_Education::getInstance();
        $params = $this->getParams();

        $education = $service->getByPK($params['id']);
        //TO Do
        if (!$education instanceof Educations_Model_Education) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Education)');
            return;
        }

        $education->fill($params);
        $education->setUpdateDate('now');
        $education->setNew(false);
        $education = $service->save($education);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($education->toArray()));
    }

    public function deleteAction()
    {
        $service = Educations_Service_Education::getInstance();
        $educationId = $this->_getParam('id');
        $education = $service->getByPK($educationId);

        if (!$education instanceof Educations_Model_Education) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Education)");
            return;
        }

        $service->remove($education);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}


<?php

class Pricedetails_Service_Pricedetail extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Pricedetails_Model_DbTable_Pricedetails();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Pricedetails_Model_Pricedetail $pricedetail = null)
    {

        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$pricedetail instanceof Pricedetails_Model_Pricedetail) {
            $pricedetail = new Pricedetails_Model_Pricedetail();
        }
        $pricedetail->fill($row);
        $pricedetail->setNew(false);
        return $pricedetail;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $select->from(array('u' => 'pricedetails'), array('*'));
        $cSelect->from(array('u' => 'pricedetails'), array('COUNT(*) AS count'));

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "u.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                    case 'updateDate':
                        $select->where("u.updateDate > '$value'");
                        $cSelect->where("u.updateDate > '$value'");
                        break;
                    default:
                        $key = "u.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                }
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $pricedetails = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $item = new Pricedetails_Model_Pricedetail();
            $item->fill($row);
            $pricedetails[] = $item;
        }

        return $pricedetails;
    }

    public function save(Pricedetails_Model_Pricedetail $pricedetail)
    {
        if ($pricedetail->isNew()) {
            $data = $pricedetail->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $pricedetail);
            }
        } else {
            $id = $pricedetail->getId();
            $data = $pricedetail->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $pricedetail);
        }

        return false;
    }

    public function remove(Pricedetails_Model_Pricedetail $pricedetail)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $pricedetail->getId());
        $this->_table->delete($where);
    }

    public function removeByConditions($conditions = array())
    {
        $where = '';
        foreach ($conditions as $property => $value) {
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $where .= $this->_table->getAdapter()->quoteInto($property . ' = ?', $value);
        }

        $this->_table->delete($where);
    }

    public function removeByQuery($property, $query)
    {
        $this->_table->delete("$property IN ($query)");
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

}

<?php

class Offices_Model_Office extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => NULL,
        'title' => NULL,
        'description' => NULL,
        'siteUrl' => NULL,
        'email' => NULL,
        'ceo' => NULL,
        'tell' => NULL,
        'fax' => NULL,
        'economicCode' => NULL,
        'creationDate' => NULL,
        'updateDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'title' :
                case 'description' :
                case 'siteUrl' :
                case 'email' :
                case 'ceo' :
                case 'tell' :
                case 'fax':
                case 'economicCode' :
                case 'creationDate' :
                case 'updateDate' :
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}


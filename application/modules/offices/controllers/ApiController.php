<?php

class Offices_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);

        $updateDate = $this->getParam("updateDate");
        $deleted = $this->_getParam('deleted');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($deleted) && $filter["deleted"] = $deleted;

        $office_service = Offices_Service_Office::getInstance();
        $offices = $office_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($offices as $item) {
            $item = $item->toArray();
            $r_service = Resources_Service_Resource::getInstance();
            $images = $r_service->getImagesByParent(array(
                "parentId" => $item["id"],
                "parentType" => "offices"
                    ));
            $item["photos"] = $images;
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Offices_Service_Office::getInstance();
        $office = $service->getByPK($id);

        if (!$office) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Office)");
            return;
        }

        $result = $office->toArray();
        $r_service = Resources_Service_Resource::getInstance();
        $images = $r_service->getImagesByParent(array(
            "parentId" => $result["id"],
            "parentType" => "offices"
                ));
        $result["photos"] = $images;

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();
        $user = $authService->getCurrentUser();
        $images = $params["images"];
//        if (!isset($user)) {
//            $this->getResponse()
//                    ->setHttpResponseCode(404)
//                    ->appendBody('Unauthorized');
//            return;
//        }

        $edcation = new Offices_Model_Office();
        $edcation->setCreationDate('now');
        $edcation->setupdateDate('now');
        $edcation->fill($params);

        $result = Offices_Service_Office::getInstance()
                ->save($edcation);
        if ($result) {
            if (count($images)) {
                foreach ($images as $img) {
                    $r_model = new Resources_Model_Resource();
                    $r_service = Resources_Service_Resource::getInstance();

                    $img['parentId'] = $result->getId();
                    $r_model->fill($img);
                    $r_model->setNew(false);
                    $r_service->save($r_model);
                }
            }
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($result->toArray()));
            return;
        }
    }

    public function putAction()
    {
        $service = Offices_Service_Office::getInstance();
        $params = $this->getParams();

        $office = $service->getByPK($params['id']);
        //TO Do
        if (!$office instanceof Offices_Model_Office) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Office)');
            return;
        }

        $office->fill($params);
        $office->setUpdateDate('now');
        $office->setNew(false);
        $office = $service->save($office);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($office->toArray()));
    }

    public function deleteAction()
    {
        $service = Offices_Service_Office::getInstance();
        $officeId = $this->_getParam('id');
        $office = $service->getByPK($officeId);

        if (!$office instanceof Offices_Model_Office) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Office)");
            return;
        }

        $service->remove($office);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}


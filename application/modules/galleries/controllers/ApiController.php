<?php

class Galleries_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $deleted = $this->_getParam('deleted');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $galleries_service = Galleries_Service_Gallery::getInstance();

        $updateDate = $this->_getParam("updateDate");
        $filter = array();

        isset($deleted) && $filter["deleted"] = $deleted;
        isset($updateDate) && $filter["updateDate"] = $updateDate;

        $galleries = $galleries_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($galleries as $item) {
            $item = $item->toArray();
            $r_service = Resources_Service_Resource::getInstance();
            $imageFilter = array(
                "parentId" => $item["id"],
                "parentType" => "gallery",
                "updateDate" => $item["updateDate"]
            );
            isset($deleted) && $imageFilter["deleted"] = $deleted;

            $images = $r_service->getImagesByParent($imageFilter);
            $item["photos"] = $images;
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Galleries_Service_Gallery::getInstance();
        $gallery = $service->getByPK($id);

        if ($gallery)
            $result = $gallery->toArray();
        else
            $result = array();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();

        $user = $authService->getCurrentUser();
//        if (!isset($user)) {
//            $this->getResponse()
//                    ->setHttpResponseCode(404)
//                    ->appendBody('Unauthorized');
//            return;
//        }

        $gallery = new Galleries_Model_Gallery();
        $gallery->setupdateDate('now');
        $gallery->fill($params);
        $result = Galleries_Service_Gallery::getInstance()
                ->save($gallery);
        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($result->toArray()));
            return;
        }
    }

    public function putAction()
    {
        $service = Galleries_Service_Gallery::getInstance();
        $params = $this->getParams();

        $gallery = $service->getByPK($params['id']);
        //TO Do
        if (!$gallery instanceof Galleries_Model_Gallery) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Gallery)');
            return;
        }

        $gallery->fill($params);
        $gallery->setUpdateDate('now');
        $gallery->setNew(false);
        $gallery = $service->save($gallery);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($gallery->toArray()));
    }

    public function deleteAction()
    {
        $service = Galleries_Service_Gallery::getInstance();
        $galleryId = $this->_getParam('id');
        $gallery = $service->getByPK($galleryId);

        if (!$gallery instanceof Galleries_Model_Gallery) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Gallery)");
            return;
        }

        $service->remove($gallery);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}


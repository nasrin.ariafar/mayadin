<?php

class External_PriceDetailController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('since', 0);
        $updateDate = $this->getParam("updateDate");
        $deleted = $this->_getParam('deleted');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($deleted) && $filter["deleted"] = $deleted;

        $dt = new DateTime($str);
        $dt->setTimeZone(new DateTimeZone('Asia/Tehran'));

        $products_service = External_Service_PriceDetail::getInstance();
        $products = $products_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($products as $item) {
            $item = $item->toArray();
            $r_service = Resources_Service_Resource::getInstance();
            $images = $r_service->getImagesByParent(array(
                "parentId" => $item["id"],
                "parentType" => "product"
                    ));
            $item["photos"] = $images;
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list,
            'updateDate' => $dt->format('Y-m-d H:i:s')
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = External_Service_PriceDetail::getInstance();
        $product = $service->getByPK($id);

        if ($product)
            $result = $product->toArray();
        else
            $result = array();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();

        $user = $authService->getCurrentUser();
//        if (!isset($user)) {
//            $this->getResponse()
//                    ->setHttpResponseCode(404)
//                    ->appendBody('Unauthorized');
//            return;
//        }

        $product = new External_Model_PriceDetail();
        $product->setupdateDate('now');
        $product->fill($params);
        $result = External_Service_PriceDetail::getInstance()
                ->save($product);
        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($result->toArray()));
            return;
        }
    }

    public function putAction()
    {
        $service = External_Service_PriceDetail::getInstance();
        $params = $this->getParams();

        $product = $service->getByPK($params['id']);
        //TO Do
        if (!$product instanceof External_Model_PriceDetail) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (PriceDetail)');
            return;
        }

        $product->fill($params);
        $product->setUpdateDate('now');
        $product->setNew(false);
        $product = $service->save($product);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($product->toArray()));
    }

    public function deleteAction()
    {
        $service = External_Service_PriceDetail::getInstance();
        $productId = $this->_getParam('id');
        $product = $service->getByPK($productId);

        if (!$product instanceof External_Model_PriceDetail) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (PriceDetail)");
            return;
        }

        $service->remove($product);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}


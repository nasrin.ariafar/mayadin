<?php

class Memories_Model_Memory extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => NULL,
        'description' => NULL,
        'title' => NULL,
        'deleted' => 0,
        'updateDate' => NULL,
        'creationDate' => NULL
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'description' :
                case 'title' :
                case 'updateDate';
                case 'creationDate';
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}


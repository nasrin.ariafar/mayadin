<?php

class Markets_Model_Market extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => NULL,
        'mrktName' => NULL,
        'mrktParent' => NULL,
        'mrktStatus' => NULL,
        'mrktAddress' => NULL,
        'mrktRegionNo' => NULL,
        'mrktAreaNo' => NULL,
        'telNumber' => NULL,
        'fruitBooth' => NULL,
        'vegetableBooth' => NULL,
        'summerCropsBooth' => NULL,
        'chickenFishBooth' => NULL,
        'meatBooth' => NULL,
        'diaryBooth' => NULL,
        'electricBooth' => NULL,
        'nutsBooth' => NULL,
        'laundryBooth' => NULL,
        'dressmakingBooth' => NULL,
        'recycleBooth' => NULL,
        'flowerBooth' => NULL,
        'plasticBooth' => NULL,
        'sausageBooth' => NULL,
        'clothingBooth' => NULL,
        'riceBooth' => NULL,
        'breadBooth' => NULL,
        'compoteBooth' => NULL,
        'courierBooth' => NULL,
        'agencyBooth' => NULL,
        'detergentBooth' => NULL,
        'stationeryBooth' => NULL,
        'otherBooth' => NULL,
        'latitude' => NULL,
        'longitude' => NULL,
        'creationDate' => NULL,
        'updateDate' => NULL,
        'parking' => NULL,
        'establishYear' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'mrktName' :
                case 'mrktParent' :
                case 'mrktStatus' :
                case 'mrktAddress' :
                case 'mrktRegionNo':
                case 'mrktAreaNo':
                case 'telNumber':
                case 'fruitBooth':
                case 'vegetableBooth':
                case 'summerCropsBooth':
                case 'chickenFishBooth':
                case 'meatBooth':
                case 'diaryBooth':
                case 'electricBooth':
                case 'nutsBooth':
                case 'laundryBooth':
                case 'dressmakingBooth':
                case 'recycleBooth':
                case 'flowerBooth':
                case 'plasticBooth':
                case 'sausageBooth':
                case 'clothingBooth':
                case 'riceBooth':
                case 'breadBooth':
                case 'compoteBooth':
                case 'courierBooth':
                case 'agencyBooth':
                case 'detergentBooth':
                case 'stationeryBooth':
                case 'otherBooth':
                case 'parking' :
                case 'latitude':
                case 'longitude':
                case 'creationDate' :
                case 'updateDate' :
                case 'deleted':
                case 'establishYear':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}


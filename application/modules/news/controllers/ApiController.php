<?php

class News_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit');
        $start = $this->_getParam('start', 0);

        $deleted = $this->_getParam('deleted');
        $updateDate = $this->getParam("updateDate");
        $type = $this->getParam("type", 0);

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($type) && $filter["type"] = $type;
        isset($deleted) && $filter["deleted"] = $deleted;

        $dt = new DateTime($str);
        $dt->setTimeZone(new DateTimeZone('Asia/Tehran'));

        $news_service = News_Service_News::getInstance();
        $news = $news_service->getList($filter, $sort, $start, $count, $limit);

        $list = array();
        foreach ($news as $item) {
            $item = $item->toArray();
            $r_service = Resources_Service_Resource::getInstance();
            $images = $r_service->getImagesByParent(array(
                "parentId" => $item["id"],
                "parentType" => "news"
                    ));
            $item["photos"] = $images;
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list,
            'updateDate' => $dt->format('Y-m-d H:i:s')
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = News_Service_News::getInstance();
        $news = $service->getByPK($id);
        $deletedImage = $this->_getParam('deletedImage');

        if (!$news) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (News)");
            return;
        }

        $result = $news->toArray();
        $r_service = Resources_Service_Resource::getInstance();
        $imageFilter = array(
            "parentId" => $result["id"],
            "parentType" => "news"
        );
        isset($deletedImage) && $imageFilter["deleted"] = $deletedImage;
        $images = $r_service->getImagesByParent($imageFilter);
        $result["photos"] = $images;

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();
        $user = $authService->getCurrentUser();
        $images = $params["images"];
        $d = new DateTime();
        if (!isset($user)) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Unauthorized');
            return;
        }
        $news = new News_Model_News();
        $news->setCreationDate('now');
        $news->setupdateDate('now');
        $news->fill($params);

        $result = News_Service_News::getInstance()
                ->save($news);

        if ($result) {
            $new_item = $result->toArray();

            if (count($images)) {
                foreach ($images as $img) {
                    $r_model = new Resources_Model_Resource();
                    $r_service = Resources_Service_Resource::getInstance();

                    $img['parentId'] = $result->getId();
                    $r_model->fill($img);
                    $r_model->setNew(false);
                    $r_service->save($r_model);
                }

                $new_item["photos"] = $images;
            }

            $this->pushActivity($new_item, 'CREATE');

            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($new_item));
            return;
        }
    }

    public function putAction()
    {
        $service = News_Service_News::getInstance();
        $params = $this->getParams();
        $action = $params['deleted'] ? 'DELETE'  : 'UPDATE';
        $news = $service->getByPK($params['id']);
        //TO Do
        if (!$news instanceof News_Model_News) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (News)');
            return;
        }

        $news->fill($params);
        $news->setUpdateDate('now');
        $news->setNew(false);
        $news = $service->save($news);

        $this->pushActivity($news->toArray(), $action);
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($news->toArray()));
    }

    public function deleteAction()
    {
        $service = News_Service_News::getInstance();
        $newsId = $this->_getParam('id');
        $news = $service->getByPK($newsId);

        if (!$news instanceof News_Model_News) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (News)");
            return;
        }

        $service->remove($news);
        $this->pushActivity($news->toArray(), 'DELETE');
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}

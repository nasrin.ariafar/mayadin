<?php

abstract class Bootstrap_Abstract extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initConfig()
    {
        $config = new Zend_Config($this->getOptions(), true);
        Zend_Registry::set('config', $config);
        date_default_timezone_set("Asia/Tehran");
    }

    protected function _initAppAutoload()
    {
        $modules = array(
            'default',
            'news',
            'users',
            'markets',
            'questionsAnswers',
            'educations',
            'galleries',
            'evaluations',
            'resources',
            'reports',
            'stations',
            'links',
            'categories',
            'external',
            'products',
            'pricedetails',
            'priceheads',
            'activities'
        );
        foreach ($modules as $module) {
            $options = array(
                'namespace' => ucfirst($module),
                'basePath' => APPLICATION_PATH . DS . 'modules' . DS . $module
            );
            $autoloader = new Zend_Application_Module_Autoloader($options);
            $autoloader->addResourceType('gearman_workers', 'workers', 'Worker');
            $autoloader->addResourceType('hooks', 'hooks', 'Hook');
            $autoloader->addResourceType('validates', 'validates', 'Validate');
        }
    }

    protected function _initLocale()
    {

    }

    protected function _initDatabase()
    {
        $this->bootstrap('db');
        $db = $this->getPluginResource('db')->getDbAdapter();
        Zend_Registry::set('db', $db);
    }

    protected function _initCache()
    {
        $options = $this->getOptions();

        if (isset($options['cache'])) {
            $cache = Zend_Cache::factory(
                            $options['cache']['frontend']['type'], $options['cache']['backend']['type'], $options['cache']['frontend']['options'], $options['cache']['backend']['options']
            );

            Zend_Registry::set('cache', $cache);
            return $cache;
        }
    }

    protected function _initLogger()
    {
        $options = $this->getOptions();
        if (isset($options['app']['use_logging'])
                && $options['app']['use_logging']
        ) {
            $logger = new Zend_Log();
            $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/logs/logs.log');
            $formatter = new Tea_Log_Formatter_Json();
            $writer->setFormatter($formatter);
            $logger->addWriter($writer);

            Zend_Registry::set('logger', $logger);
        }
    }

    protected function _initHooks()
    {
        $options = $this->getOptions();

        if (isset($options['events'])) {
            foreach ($options['events'] as $event => $data) {
                foreach ($data['hooks'] as $hook) {
                    Tea_Hook_Registry::addHook($event, $hook);
                }
            }
        }
    }

    protected function _initMail()
    {
        $mailConfig = Zend_Registry::get('config')->mail;
        if (!isset($mailConfig->transporter)) {
            return;
        }
        if ($mailConfig->transporter == 'smtp') {
            $transport = new Zend_Mail_Transport_Smtp(
                            $mailConfig->smtpconfig->host,
                            $mailConfig->smtpconfig->toArray()
            );
        } else {
            $transport = new Zend_Mail_Transport_Sendmail();
        }

        if (isset($transport)) {
            Zend_Mail::setDefaultTransport($transport);
        }
    }

    protected function _initNoSql()
    {
        $options = $this->getOptions();
        if (isset($options['nosql']['neo4j'])) {
            try {
                $neo4j = $options['nosql']['neo4j'];
                $trans = new Tea_Neo4j_Transport($neo4j['host'], $neo4j['port']);
                $client = new Tea_Neo4j_Client($trans);
                Zend_Registry::set('neo4j', $client);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        if (isset($options['nosql']['redis'])) {
            try {
                $host = isset($options['nosql']['redis']['host']) ? $options['nosql']['redis']['host'] : 'localhost';
                $port = isset($options['nosql']['redis']['port']) ? $options['nosql']['redis']['port'] : '6379';

                $redis = new Redis();
                $redis->connect($host, $port);
                Zend_Registry::set('redis', $redis);
            } catch (RedisException $e) {
                die($e->getMessage());
            }
        }
    }

}

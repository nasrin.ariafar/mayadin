define([
    'backbone',
    'underscore',
    'jquery',
    ], function (Backbone, _, $) {

        var application = function () {

            return {
            
//z                ajaxStart: function() {
//                    $(document).bind({
//                        ajaxStart: function(pa,re) {
//                            alert("waiting")
//                        },
//                        ajaxStop: function() {},
//                        ajaxError: function(e, error) {
//                            switch (error.status) {
//                                case 401:
//                                    $('#alert-login').modal('show');
//                                    break;
//                            }
//                        }
//                    });
//                },
            
                notify : function(type, title, message, options) {
                    var defaults = {
                        styling: "bootstrap",
                        type : "notice",
                        delay: 5000,
                        hide : true
                    };

                    var options = options || {};
                    var options = _.extend(defaults, options);

                    options.title = title;
                    options.text  = message;
                    options.type  = type;

                    $.pnotify(options);
                },

                Modals : {
                    alert: function(message, callback) {
                        bootbox.alert(message, callback);
                    },

                    confirm : function(message, callback) {
                        bootbox.confirm(message, callback);
                    },

                    prompt : function(message, callback) {
                        bootbox.prompt(message, callback);
                    }
                },
                    

                Events : _.extend({}, Backbone.Events),


                Models : {
                    State  : Backbone.Model.extend({
                        defaults: {
                        },
                        setSection: function(section) {
                            this.set('section', section);
                        }
                    })
                }


            };
        }

        return application();
    });

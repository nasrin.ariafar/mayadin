define([
    'jquery',
    'backbone',
    'underscore',
    'frontend/models/item'
    ], function ($, Backbone, _, ItemModel) {

        var ProductModel = ItemModel.extend({

            defaults : {
                tableId : null,
                categiryId : null,
                values : ""
            },
            
            url : function() {
                return '/api/categories/'+ this.get('categoryId') + '/tables/' + this.get("tableId") + '/products/' + (this.get("id") || "")
            },

            parse : function(resp){
                var values = $.parseJSON(resp.values);
                resp.values = values;
                return resp;
            }
            
        });

        return ProductModel;
    });

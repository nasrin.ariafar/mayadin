define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/Collection',
    'frontend/models/activity'
    ], function ($, _, Backbone, BaseCollections, ActivityModel) {

        var ActivitiesCollection = BaseCollections.extend({
            
            model : ActivityModel,
            
            url : function(){
                return "/api/activities";
            },

            parse: function( resp ){
                var list = resp.list;

                _.each(list, function(item){
                    if(item.photos && item.photos.length > 0){
                        item.photos = $.grep(item.photos, function(item){
                            return (item.deleted != 1)
                        })
                    }
                });
                return list;
            }

        });

        return ActivitiesCollection;
    });

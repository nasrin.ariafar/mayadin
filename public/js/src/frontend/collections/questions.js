define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/Collection',
    'frontend/models/question'
    ], function ($, _, Backbone, BaseCollections, QModel) {

        var QCollection = BaseCollections.extend({
            
            model : QModel,

            initialize: function(models, opt){
                
                this.opt = opt;
                
                QCollection.__super__.initialize.apply(this, [this.model, opt]);
            },
	
            url : function(){
                return "/api/evaluations/" + this.opt.avaluationsId + "/questions";
            },

            parse: function( resp ){
                var target = this.target,
                list = resp.list;
                
                _.each(list, function(item){
                    item.target = target;
                });
                return list;
            }

        });

        return QCollection;
    });

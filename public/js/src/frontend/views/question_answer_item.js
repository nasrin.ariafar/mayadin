
define([
    'jquery',
    'underscore',
    'backbone',
    'frontend/views/item',
    'frontend/models/item',
    ], function ($, _, Backbone, ItemView, ItemModel) {

        var QuestionAnswerView = ItemView.extend({

            initialize : function(options) {
                this.events =_.extend({
                    'click .set_answer'    : 'showAnswerInput',
                    'keyup textarea'       : 'autoExpand',
                    'keydown textarea'     : 'setAnswer',
                }, this.events);

                QuestionAnswerView.__super__.initialize.call(this, options);
            },

            showAnswerInput : function(){
                $(".set_answer", this.$el).hide();
                $(".answer_input", this.$el).show();
            },

            autoExpand: function(e){
                var text = this.$('textarea'),
                rows = text.val().split('\n').length;
                text.attr('rows', rows);
            },
            
            setAnswer : function(e){
                var $this = this,
                answer = $("textarea", this.$el).val();
                
                if( e.keyCode == 13 && answer != ""){
                    this.model.save({
                        answer   : answer
                    },{
                        success : function(resp){
                            $this.render();
                        },
                        error : function(error){
                            console.log(error);
                        }
                    });
                }
            }
        });

        return QuestionAnswerView;
    });

define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'frontend/views/item_form',
    'frontend/models/item',
    'text!frontend/templates/user_form.html',
    'text!frontend/templates/image_mini.html',
    'i18n!nls/labels',
    'libs/browserplus-min',
    'libs/jqueryPlugins/plupload.full',
    'libs/jqueryPlugins/jquery.plupload.queue',
    'libs/jqueryPlugins/jquery.lightbox-0.5',
    'bootstrap_selectpicker',
    "jquery-helper",
    'pwt-date',
    'pwt-datepicker.min',
    ], function($, Backbone, _, App, FormView, ItemModel,FormTpl, ImgTpl, Labels) {
        var BoorsaneFormView = FormView.extend({

            className : 'user-form-wrapper',

            events : {
                'change .boursane-parts'    : 'changePart',
                'click .delete-photo'       : 'deletePhoto',
                'click .save'               : 'save',
                'keydown .datePicker'       : 'keydownDatepicker'
            },

            initialize: function(opt) {
                var $this = this;
                this.part = "indicesChange";
                this.images = [];

                if(this.model.get("id")){
                    this.model.fetch({
                        async : true,
                        success : function(model){
                            $this.render()
                        },
                        error : function(){
                        }
                    })
                }else{
                    this.is_New = true;
                    this.render();
                }
            },

            render : function(){
                var tpl = _.template(FormTpl);
                this.$el.html(tpl(_.extend(this.model.toJSON(), {
                    labels          : Labels,
                    target          : this.model.get("target")
                })));
                $("#main-container").html(this.$el);

                this.afterRender();
            },

            afterRender : function(){
                $(".datePicker").each(function(index,el){
                    if($(el).val() && $(el).val() != "" ){
                        var date = $(el).val() || new Date().toISOString().substr(0, 19);
                        $(el).val(_.getPersioanDate(date));
                    }
                })

                var $this = this;
                $(".datePicker", this.$el).pDatepicker({
                    autoClose : true,
                    persianDigit : true,
                    onSelect : function(m){
                        $this.selectedDate = persianDate(m).gDate ;
                        var d = new Date(m);
                        var m  = d.getTime();
                        
                        var date = persianDate(m).format("YYYY/MM/DD");
                        this.inputElem.val(date);

                        var id =  this.inputElem.attr("id");

                        m = new Date(m)

                        var dd = m.getDate();
                        var mm = m.getMonth()+1;
                        var yyyy = m.getFullYear();
                        var g_date  =  yyyy + '/' + mm + '/' + dd

                        $("input[name="+ id +"]").val(g_date);
                    }
                });

                this.initUploader();
            },

            keydownDatepicker : function(e){
                var el = $(e.target);
                el.pDatepicker( "show" );
                e.preventDefault();
            },

            changePart : function(){
                var content =  $("#content", this.$el).val();
                this.model.set(this.part, content);
                $("#content", this.$el).val("");
                $(".images-list", this.$el).empty();
                this.part = $(".boursane-parts", this.$el).val();
                if(!this.images){
                    this.images = [];
                }

                this.loadData();

            },

            loadData : function(){
                var $this = this;
                if(this.model.get(this.part) && this.model.get(this.part) != ""){
                    $("#content", this.$el).val(this.model.get(this.part));
                }

                var photos = this.images;
                if(photos && photos.length > 0){
                    _.each(photos, function(photo){
                        $this.addPhoto(photo);
                    })
                }

            },

            initUploader : function()
            {
                var $this = this;

                var uploaderBTn = new plupload.Uploader({
                    runtimes            : 'html5,html4',
                    browse_button       : 'image_upload_btn',
                    container           : 'btn_container',
                    multi_selection     : true,
                    max_file_size       : '10mb',
                    url                 :'/api/resources/?parentType=humans',
                    filters             : [{
                        title      : "Image files",
                        extensions : "jpg,gif,png"
                    }],
                    resize              : {
                        width           : 320,
                        height          : 240,
                        quality         : 90
                    },
                    init                : {
                        FilesAdded: function(up, files) {
                            $("span.plupload_upload_status", $this.$el).html("");
                            $("div.progress-bar", $this.$el).width(0);
                            $(".upload-image-progress", $this.el).show();
                            up.start();
                        },
                        beforeUpload : function(uploader ,file){
                            uploader.settings.url = '/api/resources?parentType='+ $this.model.get("target") +'&parentId=' + $this.model.get("id");
                        },
                        UploadProgress : function(up, file){
                            $("div.progress-bar", this.$el).html(up.total.percent + "%");
                            $("div.progress-bar", $this.$el).css("width", up.total.percent + "%");
                            $("span.plupload_upload_status", $this.$el).html("Uploaded "+(up.total.uploaded + 1) + "/" + up.files.length+" files");
                        },
                        FileUploaded : function(up, file, resp){
                            var photo = _.extend($.parseJSON(resp.response));
                            $this.model.set("photoId", photo.id);
                            $(".user_img", $this.$el).attr("src", "/r/"+ photo.id)
                        }
                    }
                });
                uploaderBTn.init();

            },
            addPhoto : function(photo){
                var tpl = _.template(ImgTpl);
                $(".images-list", this.$el).append(tpl(photo));
            },

            save : function(){
                if(!this.validate())
                    return;

                $(".error").removeClass("error");

                var $this = this;
                var data = $("form", this.$el).serializeJSON();

                this.model.save(_.extend(data, {
                    images : $this.images
                }), {
                    success : function(){
                        $(".loading-image").hide();
                        $(".success-msg").show();
                        setTimeout(function(){
                            $(".success-msg").hide();
                            window.history.back();
                        }, 2000);
                    //                        if($this.is_New) {
                    //                            var target = $this.model.get("target");
                    //                            var type = $this.model.get("type");
                    //                            $this.model = new ItemModel({
                    //                                target : target,
                    //                                type    : type
                    //                            });
                    //                            window.history.back();
                    //                        //                            $this.render();
                    //                        //                            $this.delegateEvents();
                    //                        }

                    }
                })

            }

        })

        return BoorsaneFormView;


    });





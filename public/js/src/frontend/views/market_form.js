define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'frontend/views/item_form',
    'text!frontend/templates/market_form.html',
    'modules/map/views/config',
    'i18n!nls/labels',

    ], function($, Backbone, _, App, ItemFormView, FormTpl, MapConfigView, Labels) {
        
        var MarketFormView = ItemFormView.extend({

            initialize: function(opt) {

                this.events = _.extend({
                    'change .location-inputs input' : 'updateMap'
                },this.events);

                var $this = this;
                this.part = "indicesChange";
                this.images = [];

                this.parts = ["indicesChange", "turnover", "maxOrder", "coinCurrencyMarket", "globalMarket", "news" , "stockMarketGroups", "commodityExchange", "futuresExchange" ];

                if(this.model.get("id")){
                    this.render()
                    if($this.model.get("photos") && $this.model.get("photos").length > 0){
                        _.each($this.model.get("photos"), function(photo){
                            $this.addPhoto(photo)
                        })
                    }
                }else{
                    this.is_New = true;
                    this.render();
                }
            },

            render : function(){
                var tpl = _.template(FormTpl),
                $this = this;
                
                this.$el.html(tpl(_.extend(this.model.toJSON(), {
                    labels          : Labels,
                    target          : this.model.get("target")
                })));

                this.mapView = new MapConfigView({
                    model : this.model,
                    onChangeLocation : function(location){
                        $("[name=latitude]").val(location.lat());
                        $("[name=longitude]").val(location.lng());
                    }
                });
                return this;
            },

            afterRender : function(){
                this.initUploader();
                this.mapView.registerEvents();
            },
            
            save : function(){
                if(!this.validate())
                    return;

                $(".error").removeClass("error");
                
                var $this = this,
                data = _.extend( $("form", this.$el).serializeJSON(), {
                    images : $this.images
                });
                //
                //                if(this.mapView && this.mapView.location){
                //                    data['latitude'] = this.mapView.location.lat();
                //                    data['longitude'] = this.mapView.location.lng();
                //                }

                this.model.save(_.extend(data,{
                    mrktParent    : (!this.collection.filters.get("mrktParent")) ? 0 : this.collection.filters.get("mrktParent")
                }), {
                    success : function(model){
                        $(".loading-image").hide();
                        $(".success-msg").show();
                        setTimeout(function(){
                            $(".success-msg").hide();
                        }, 2000);
                        $this.collection.add(model);
                        $("#new-item-modal").modal("hide");
                    }
                })
            },

            updateMap : function(){
                this.model.set({
                    latitude    : $("[name=latitude]").val(),
                    longitude   : $("[name=longitude]").val()
                });

                $('.gmap3', this.$el).gmap3({
                    map:{
                        options:{
                            center: [ $("[name=latitude]").val(), $("[name=longitude]").val()], //---place the lat and lon of map's center position
                            zoom: 15
                        }
                    },
                    marker:{
                        latLng: [ $("[name=latitude]").val(), $("[name=longitude]").val()],  //---place the lat and lon of marker
                        callback: function(){
                        //----
                        }
                    }
                });
            }
        })

        return MarketFormView;
    
   
    });





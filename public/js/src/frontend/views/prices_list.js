define([
    'application',
    'jquery',
    'underscore',
    'backbone',
    'base/views/base_view',
    'text!frontend/templates/prices_list.html',
    'frontend/collections/categories',
    'frontend/collections/tables',
    'text!frontend/templates/category_item.html',
    'text!frontend/templates/table_item.html',
    'text!frontend/templates/product_item.html',
    'text!frontend/templates/category_form.html',
    'frontend/views/market_form',
    'frontend/views/item',
    'frontend/models/market',
    'frontend/models/product',
    'frontend/models/category',
    'i18n!nls/fa-ir/labels',
    ], function (Application, $, _, Backbone, BaseView,  ListTpl, CategoriesCollection, TabelsCollection, CategoryTpl, TableTpl, ProductTpl, CategoryFormTpl, FormView, ItemView, ItemModel, ProductModel, CategoryModel, Labels) {
       
        var NewsListView = BaseView.extend({

            className : "prices-list row",
            events : {
                'click .show-category-form' : 'showCategoryForm',
                'click .create-category'    : 'createUpdateCategory',
                'click .category-item'      : 'selectCategory',
                'click .edit-category'      : 'showCategoryForm',
                'keyup .new-field'          : 'addNewField',
                'click .create-table'       : 'createTable',
                'keyup td input'            : 'createUpdateProduct',
                'click .edit-product'       : 'switichToEditMode',
                'click .delete-product'     : 'deleteProduct'
            },

            initialize : function(){
                var $this = this;
                this.tableFields = [];
                this.categoriesCollection = new CategoriesCollection([]);
                this.categoriesCollection.fetch({
                    success : function(collection){
                        $this.selectedcategory = collection.at(0);
                        $this.getTabels(collection.at(0).get("id"));
                        collection.each(function(model, index){
                            $this.renderCategory(model, index);
                        });
                    }
                })
            },

            render : function(fields){
                var $this = this;

                this.template = _.template(ListTpl);
                this.$el.html(this.template( {
                    labels  : Labels
                }));

                return this;
            },

            registerEvents : function(){
                var $this = this;

                $('#new-category').on('hidden.bs.modal', function (e) {
                    $this.categoryUploaderBTn.destroy();
                });

                $('#new-category').on('shown.bs.modal', function (e) {
                    $this.initUploader();
                });

                $("#new-table").on('hidden.bs.modal', function (e) {
                    $("textarea.table-description", $this.$el).val("");
                    $(".fields-list", $this.$el).empty();
                    $(".new-field", $this.$el).val("");
                    $this.tableFields = [];
                });

            },

            showCategoryForm : function(e){
                var id = $(e.target).parents("li").attr("id");
                var formTpl = _.template(CategoryFormTpl);
                this.newcategoryModel = (id) ? this.categoriesCollection.get(id) : new CategoryModel();
                $("#new-category", this.$el).html(formTpl(_.extend(this.newcategoryModel.toJSON(), {
                    labels : Labels
                })));

                $("#new-category", this.$el).modal("show");
            },
            
            initUploader : function(model){
                
                var $this = this;

                this.categoryUploaderBTn = new plupload.Uploader({
                    runtimes            : 'html5,html4',
                    browse_button       : 'file_upload_btn',
                    container           : 'upload_btn_container',
                    multi_selection     : true,
                    max_file_size       : '10mb',
                    url                 :'/api/resources/',
                    resize              : {
                        width           : 320,
                        height          : 240,
                        quality         : 90
                    },
                    filters             : [{
                        title      : "Image files",
                        extensions : "jpg,gif,png"
                    }],

                    init                : {
                        FilesAdded: function(up, files) {
                            $("span.plupload_upload_status", $this.$el).html("");
                            $("div.progress-bar", $this.$el).width(0);
                            $(".upload-image-progress", $this.el).show();
                            up.start();
                        },
                        beforeUpload : function(uploader ,file){
                            uploader.settings.url = '/api/resources?parentType=category'+ (($this.newcategoryModel.get("id")) ? '&parentId=' + $this.newcategoryModel.get("id") : "" );
                        },
                        UploadProgress : function(up, file){
                            $("div.progress-bar", this.$el).html(up.total.percent + "%");
                            $("div.progress-bar", $this.$el).css("width", up.total.percent + "%");
                            $("span.plupload_upload_status", $this.$el).html("Uploaded "+(up.total.uploaded + 1) + "/" + up.files.length+" files");
                        },
                        FileUploaded : function(up, file, resp){
                            if(up.total.uploaded == up.files.length){
                                $('.upload-image-progress', this.$el).fadeOut(5000);
                            }
                            var file = _.extend($.parseJSON(resp.response));
                            $(".image-preview").attr("src", file.location);
                            $this.newcategoryModel.set("photo", file);
                        }
                    }
                });
                this.categoryUploaderBTn.init();

            },

            renderCategory : function(category, index){
                var tpl = _.template(CategoryTpl),
                el = tpl(_.extend(category.toJSON(),{
                    index : index
                }));
                
                if($('.category-item#' + category.get("id")).length){
                    $('.category-item#' + category.get("id")).replaceWith(el);
                    if(this.selectedcategory.get("id") == category.get("id"))
                        $('.category-item#' + category.get("id")).addClass("selected");
                }else{
                    $('.categories-list', this.$el).append(el);
                }
                
            },
            
            createUpdateCategory : function(){
                var title =   $("#new-category input.new-category").val(),
                $this = this,
                is_new = (this.newcategoryModel.get("id")) ? false : true;
                
                if (title != ""){
                    this.newcategoryModel.save({
                        title : title
                    }, {
                        success : function(model){
                            is_new &&  $this.categoriesCollection.add(model);
                            $this.renderCategory(model);
                            $("#new-category", $this.$el).modal("hide");
                        }
                    })
                }
               
            },

            selectCategory : function(e){
                if($(e.target).closest(".edit-category").length)
                    return;

                var el = $(e.target).closest('.category-item'),
                categoryId = el.attr("id");

                if(!el.hasClass("selected")){
                    $(".categories-wrapper ul .selected").removeClass("selected");
                    el.addClass("selected");
                    $(".tables-list", this.$el).empty();
                    this.selectedcategory = this.categoriesCollection.get(categoryId);
                    this.getTabels(el.attr("id"));
                }

            },

            popNewItemCreation : function(model){
                var form_view = new FormView({
                    collection  : this.collection,
                    model       : (model && model instanceof Backbone.Model) ? model :  new ItemModel(),
                    className   : "modal-dialog"
                });

                $("#new-item-modal", this.$el).html(form_view.$el);
                $("#new-item-modal", this.$el).modal();
                form_view.afterRender();
            },

            popEditItem : function(e){
                var item_id = $(e.target).parents(".item-wrapper").attr("id"),
                model = this.collection.get(item_id);
                this.popNewItemCreation(model);
            },
            
            afterRenderItems : function(){
                var $this = this;

                if(this.collection.length == 0)
                    return;
            },

            getNewsByType : function(e){
                var el = $(e.target),
                type = el.data("type");

                if(!el.hasClass("selected")){
                    $(".second-navbar .selected", this.$el).removeClass("selected");
                    el.addClass("selected");
                    this.collection.filters.set("type", type);
                }
            },

            showFolderItems : function(e){
                var item_id = $(e.target).parents(".item-wrapper").attr("id"),
                parent = this.collection.get(item_id);
                
                $(".title", this.$el).html(Labels.square_bazaar_list + parent.get("name"))
                this.collection.filters.set("parentId", item_id);
                $(".back", this.$el).show();
            },

            back : function(e){
                this.collection.filters.set("parentId", 0);
                $(".back", this.$el).hide();
                $(".title", this.$el).html(Labels.squares_list);
            },

            addNewField : function(e){
                var field = $(".new-field", this.$el).val();
                if(field != "" && e.keyCode == 13){
                    this.tableFields.push(field);
                    $(".fields-list").append('<li>' + field + '</li>');
                    $(".new-field", this.$el).val("");
                }
            },

            getTabels : function(categoryId){
                var $this = this;
                
                this.tabelsCollection = new TabelsCollection([], {
                    categoryId : categoryId
                });
                this.tabelsCollection.fetch({
                    success : function(collection){
                        collection.each(function(model, index){
                            $this.renderTable(model, index);
                        });
                    }
                });
            },

            createTable : function(){
                var $this = this;
                if(!this.tableFields.length)
                    return;
                
                this.tabelsCollection.create({
                    categoryId : this.selectedcategory.get("id"),
                    description : $(".table-description").val(),
                    fields : this.tableFields
                },{
                    success : function(model){
                        $("#new-table").modal("hide");
                        $this.renderTable(model);
                    }
                });
            },

            renderTable : function(table){
                var tpl = _.template(TableTpl),
                $this = this;
                
                $('.tables-list', this.$el).append(tpl(_.extend(table.toJSON(),{
                    labels : Labels
                })));

                _.each(table.get("products"), function(product, index){
                    $this.renderRow(product, index)
                });
            },

            createUpdateProduct : function(e){
                var row = $(e.target).parents("tr"),
                tableId = row.data("table"),
                categoryId = row.data("category"),
                productId = row.attr("id"),
                values = [],
                product = new ProductModel(),
                $this = this,
                action = (productId) ? "edit" : 'create';
                
                if(e.keyCode != 13 || $("input[name=value]", row).val() == "" )
                    return;


                _.each($("form", row), function(form){
                    values.push($(form).serializeJSON());
                });

                product.save({
                    id      : productId,
                    tableId : tableId,
                    categoryId : categoryId,
                    values : JSON.stringify(values)
                },{
                    success : function(model){
                        if(action == "create"){
                            $("input:not(:hidden)", row).val("");
                            $this.renderRow(model.toJSON());
                        }else{
                            _.each($(".key-value", row), function(el){
                                var val =  $("input[name=value]", $(el)).val();
                                $("span.value", $(el)).show().html(val);
                                $("input[name=value]", $(el)).hide();
                            });
                        }
                    }
                })
                
            },

            renderRow : function(product, index){
                var tpl = _.template(ProductTpl);
                $('#' + product.tableId + ' tbody', this.$el).append(tpl(_.extend(product, {
                    index : (index) ? (index + 1) : $('#' + product.tableId + ' tbody tr', this.$el).length
                })));
            },

            switichToEditMode : function(e){
                var tr = $(e.target).parents("tr"),
                productId = tr.attr("id");
                _.each($(".key-value", tr), function(el){
                    $("span", $(el)).hide();
                    $("input[name=value]", $(el)).show();
                });
            },
            
            deleteProduct : function(e){
                var row = $(e.target).parents("tr"),
                product = new ProductModel({
                    id : row.attr("id"),
                    tableId :  row.data("table"),
                    categoryId : row.data("category")
                });

                product.destroy({
                    success : function(){
                        row.remove();
                    }
                })
            }

        });


        return NewsListView
    });

define([
    'jquery',
    'backbone',
    'underscore',
    'pwt-date',
    'pwt-datepicker.min',
    ], function($, Backbone, _) {
        window._views = [];

        var BaseView = Backbone.View.extend({
            views : [],

            constructor : function(){
                Backbone.View.apply(this, arguments);
                this.$el.data('removable', true);
                window._views.push(this.$el);
                this.views.push(this);
            },

            setRemovable : function(isRemovable){
                this.isRemovable = isRemovable;
            },

            dispose: function() {
                this.unbindFromAll();
                this.unbind();
                this.remove();
            },

            getRemovable : function(){
                return this.isRemovable;
            },

            setVal : function(e){
                var el = $(e.target),
                value = this.getNumber(el.val());
                if(value.length > 0){
                    var val = _.translate(value, true);
                    el.val(val);
                }
            },

            getNumber : function(value){
                value =  value.replace(/,/g, "");
                
                if(value){
                    value = value.replace(/۰/g, 0);
                    value = value.replace(/۱/g, 1);
                    value = value.replace(/۲/g, 2);
                    value = value.replace(/۳/g, 3);
                    value = value.replace(/۴/g, 4);
                    value = value.replace(/۵/g, 5);
                    value = value.replace(/۶/g, 6);
                    value = value.replace(/۷/g, 7);
                    value = value.replace(/۸/g, 8);
                    value = value.replace(/۹/g, 9);
                }

                return value;
            }
           
        });

        _.mixin({
            ucFirst : function(str) {
                if (typeof str != "string") {
                    return str;
                }
                return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
            },
            translate : function convert(string, seperate) {
                
                if(string && string != "NULL"){
                    var string = string.toString();

                    if(seperate){
                        while (/(\d+)(\d{3})/.test(string.toString())){
                            string = string.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                        }
                    }

                    if(string){
                        var persian_numbers = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
                        string = string.replace(/0/g, persian_numbers[0]);
                        string = string.replace(/1/g, persian_numbers[1]);
                        string = string.replace(/2/g, persian_numbers[2]);
                        string = string.replace(/3/g, persian_numbers[3]);
                        string = string.replace(/4/g, persian_numbers[4]);
                        string = string.replace(/5/g, persian_numbers[5]);
                        string = string.replace(/6/g, persian_numbers[6]);
                        string = string.replace(/7/g, persian_numbers[7]);
                        string = string.replace(/8/g, persian_numbers[8]);
                        string = string.replace(/9/g, persian_numbers[9]);
                    }
                    return string;
                }
                else return "-";
                   
            },
            convertNumber : function(number){
                var KILO = 1000;
                var MEGA = 1000000;
                var GIGA = 1000000000;
                if(number < KILO){
                    return _.translate(number);
                }
                else if ( number < MEGA){
                    return _.translate(number / KILO) + " هزار "
                } else if (number < GIGA){
                    return _.translate( number / MEGA) + " میلیون "
                } else {
                    return _.translate(number / GIGA) + " میلیارد "
                }
            },

            getPersioanDate : function(gDate){
                var t = new Date(gDate.slice(0,10)).getTime();
                return new persianDate(t).format("YYYY/MM/DD");
            },

            getPersioanDateTime : function(gDate){
                debugger
                var t = new Date(gDate.slice(0,10)).getTime();
                return new persianDate(t).format("YYYY/MM/DD");
            },

        });

        return BaseView;
    });

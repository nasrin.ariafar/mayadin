define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'base/views/base_view',
    'libs/jqueryPlugins/waypoints',
    'i18n!nls/labels'
    ], function($, Backbone, _, App, BaseView, waypoints, labels) {
        
        var BaseListView = BaseView.extend({

            autoLoadMore : true,

            initialize : function( opt) {
                _.bindAll(this, 'render', 'renderItem', 'fetchCollection', 'updatePaging');
                this.items = [];
                
                var self = this;

                this.collection.fetch({
                    add     : true,
                    silent  : true
                });

                this.collection.on('reset', this.reset, this);
                
//                this.collection.on('add' , this.renderItem, this);

                this.collection.on('fetchSuccess', function(resp, collection){
                    $('.hold-on-loading').css('display', 'block');
                    if (self.autoLoadMore) {
//                        $('.hold-on-loading').waypoint('destroy');
                        var count = (self.collection.filters.count) ? self.collection.filters.count : -1;
                        if(!count || parseInt(self.collection.filters.get('start')) + parseInt(self.collection.filters.get('limit')) <= count){
                            $('.hold-on-loading').waypoint(function(event, direction) {
                                if (direction == 'down'){
                                    self.loadMore();
                                    $('.hold-on-loading').css('display', 'block');
                                }else{
                                    $('.hold-on-loading').css('display', 'none');
                                }
                            }, {
                                offset: function() {
                                    return $.waypoints('viewportHeight') - $(this).outerHeight();
                                },
                                triggerOnce: false,
                                onlyOnScroll: true
                            });
                        }
                    }
                    else{
                        $('.cr-pagination').css('visibility', 'visible');
                    }

                    for(var i = this.filters.get('start'); i < this.models.length; i++){
                        self.renderItem(this.models[i]);
                    }
                    self.afterRenderItems();
                    self.updatePaging();
                });
            },

            events : {
                'click      .cr-pagination'      : 'loadMore'
            },


            reset : function(){
                _.each(this.items, function(itemView){
                    itemView.dispose();
                })
            },
            
            beforeRenderItems : function(url){
            },
        
            render : function(){
                this.beforeRenderItems();
                this.collection.fetch({
                    add     : true,
                    silent  : true
                });
            },

            renderItem : function(){},

            afterRenderItems : function(){},

            fetchCollection : function(){
                $('.list',this.$el).empty();
                this.collection.reset();

                this.collection.fetch({
                    add: true,
                    silent : true
                });
            },

            loadMore: function(){
                this.collection.increase().fetch({
                    add: true,
                    silent : true
                });
            },

            updatePaging: function(){
    
                if(this.collection.filters.get('start') + this.collection.filters.get('limit') > this.collection.filters.count){
                    $('.hold-on-loading').html('<p><span>' + labels.NoItems + '</span></p>');
                }else{
                    $('.hold-on-loading').html('<p><span>' +labels.Holdon +'</span></p>');
                }
                if(this.collection.models.length < 1){
                    $('.hold-on-loading').html('<p><span>' + labels.Noitems + '</span></p>');
                }
            },

            updateMenuTitle : function(e){
                var parent = $(e.target).parents('.menu');
                $('.btn-order-label' , parent).html(($(e.target).html()));
            }
        });

        return BaseListView;
    });

define([
    'jquery',
    'backbone',
    'underscore',
    'base/views/base_view'
    ], function($, Backbone, _, BaseView) {
        window._views = [];
        
        
        var BaseItemView = BaseView.extend({
            dispose : function(){
                this.unbind();
                this.remove();
            }
        });
       
        return BaseItemView;
    });

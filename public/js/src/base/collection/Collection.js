define([
    'jquery',
    'underscore',
    'backbone',   
    'base/model/FilterModel'
    ], function ($, _, Backbone, FilterModel) {
        var BaseCollections = Backbone.Collection.extend({
            model:  new Backbone.Model(),   
	
            initialize: function(models, opt){
                this.opt = opt;
                var $this = this;
                
                this.filters = new FilterModel();

                this.filters.on('change', function() {
                    $this.reset()
                    $this.fetch({
                        add  : true
                    });
                });
            },
	
            parse: function( resp ){
                if(resp && this.filters){
                    this.filters.count = resp.count ;
                }                       
                return resp.list;
            },
	
            setFilters: function(filters){        
                var limit = this.filters.get('limit');
                var start = filters.start ;
        
                this.filters.clear({
                    silent : true
                }) 
                filters.limit = limit;
                filters.start = start;
                this.filters.set(filters);
            },
	
            increase: function( count ){
                this.filters.set('start', this.filters.get('start') + (count ? count : this.filters.get('limit')));
                return this;
            },
    
            getOpt: function(){
                return this.opt;
            },
	
            fetch: function(options){   
                options || (options = {});

                // this extention done for new updates of backbone

                $.extend(options, {
                    update     : true,
                    remove     : false,
                    type       : (this.type) ? this.type : "GET"
                });

                if(this.filters){
                    var filters = this.filters.toJSON();
                    if(options.data){
                        for( var i in filters ){
                            options.data[i] = filters[i];
                        }		
                    }
                    else{
                        options.data = filters;
                    }
                }
                else{
                //            console.log('filters is undefined');
                }
                var collection = this;
                var success = options.success;
                options.success = function( collection, resp, xhr){
                    collection.trigger('fetchSuccess', resp, collection);                 
                    if (success) success(collection, resp);
			
                }
                return Backbone.Collection.prototype.fetch.call(this, options);
            }
		
        });

        return BaseCollections;
    });

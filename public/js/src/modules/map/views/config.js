define([
    'jquery',
    'underscore',
    'backbone',
    //    'backbone.googlemaps',
    'map',
    'text!modules/map/templates/config.html',
    'i18n!modules/map/nls/fa-ir/labels'
    ], function ($, _, Backbone, Map, ConfigsTpl, labels) {
        var SnippetView = Backbone.View.extend({

            initialize: function() {
                var obj  = _.extend(this.model.toJSON(), {
                    labels : labels
                });
                var tpl  = _.template(ConfigsTpl, obj)
                this.$el = $(tpl);
                return this;
            },

            
            locationSearch : function(e){
                if(e.which == 13) {
                    var location = $(e.target).val();
                    google.searchLocation(location, this.$el);
                    return false;
                }
            },

            showZone : function(){
                var $this = this;
                
                function onClk(circle, event, context){
                    var $e = $('#'+context.data);
                    if ($e.hasClass('clicked')) {
                        $e.removeClass('clicked').css('backgroundColor', '#FFFFFF');
                    } else {
                        $e.addClass('clicked').css('backgroundColor', circle.strokeColor);
                    }
                }

                var location = [35.744, 51.3750];

                var address = this.model.get('cityName') + ' ' + this.model.get('districtName');
                $(".gmap3").gmap3({
                    map:{
                        options:{
                            center: location,
                            zoom:15,
                        }
                    },
                    marker:{
                        latLng: [$this.model.get('latitude'), $this.model.get('longitude')],
                        address : address,
                        options:{
                            draggable : false
                        }
                    },
                    circle:{
                        values:[
                        {
                            options:{
                                center: location,
                                radius : 300,
                                fillColor : "#FFAF9F",
                                strokeColor : "#FF512F"
                            },
                            data:"box1"
                        },
                        {
                            options:{
                                center: location,
                                radius : 200,
                                fillColor : "#F4AFFF",
                                strokeColor : "#CB53DF"
                            },
                            data: "box2"
                        }
                        ],
                        events:{
                            click: onClk
                        }
                    }
                });

                // second call : create a single cicle
                $('#test1').gmap3({
                    circle:{
                        options:{
                            center: location,
                            radius : 100,
                            fillColor : "#008BB2",
                            strokeColor : "#005BB7"
                        },
                        data: "box3",
                        events:{
                            click: onClk
                        }
                    }
                });
            },


            registerEvents : function(){
                
                var $this = this;
                //                var address = (this.model.get('cityName') || "تهران ") + ' ' + (this.model.get('districtName') ||  "برج میلاد");
                //
                //                if(this.model.get('latitude') == -1  && this.model.get('longitude') == -1 ){
                //                    this.showZone();
                //
                //                }else{
                var location = [($this.model.get('latitude') || 35.744),  ($this.model.get('longitude')|| 51.3750)];

                map =  $(".gmap3").gmap3({
                    marker:{
                        latLng: location,
                        options:{
                            draggable:true
                        },
                        events:{
                            dragend: function(marker){
                                $(this).gmap3({
                                    getaddress:{
                                        latLng:marker.getPosition(),
                                        callback:function(results){
                                            $this.location =  results[0].geometry.location;
                                            $this.options.onChangeLocation && $this.options.onChangeLocation($this.location);
                                        }
                                    }
                                });
                            }
                        }
                    },
                    map:{
                        options:{
                            center : location,
                            zoom: 15,
                            mapTypeControl : true,
                            mapTypeControlOptions: {
                                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                            },
                            navigationControl: true,
                            scrollwheel: true,
                            streetViewControl: true,
                            zoomControl: true,
                            zoomControlOptions: {
                                position: google.maps.ControlPosition.RIGHT_TOP,
                                style: "SMALL"
                            },
                            panControl: false,
                            panControlOptions: {
                                position: google.maps.ControlPosition.RIGHT_TOP,
                                style: "SMALL"
                            }
                        }
                    }
                });
            //                }
            }

        });

        return SnippetView;
    });

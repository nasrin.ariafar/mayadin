define([
    'jquery',
    'underscore',
    'backbone',
    'modules/resources/models/resource',
    'base/collection/Collection',
    ], function ($, _, Backbone, ResourceModel, BaseCollections) {
        var ResourceCollection = BaseCollections.extend({
            model : ResourceModel,

            url   : function(){
                if(this.albumId){
                    return 'api/albums/' + this.albumId + '/resources';
                } else {
                    return 'api/resources/';
                }
            
            },

            parse: function( resp ){ 
                if(resp && this.filters){
                    this.filters.count = resp.count ;
                }
                return resp.list;
            }
        });

        return ResourceCollection;
    });

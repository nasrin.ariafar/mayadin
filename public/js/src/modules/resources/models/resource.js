define([
    'jquery',
    'underscore',
    'backbone',
], function ($, _, Backbone) {

    var resource = Backbone.Model.extend({
        defaults : {
            name        : null,
            file_size   : null,
            file_type   : null,
            caption     : null,
            meta_data   : ''
        },

        url : function() {
            if (this.collection.albumId) { 
                return 'api/albums/' + this.collection.albumId + '/resources/' + this.get('id');
            } else {
                return 'api/resources/' + this.get('id');
            }
        },

        initialize: function() {
        }

    });

    return resource;
});

define([
    'jquery',
    'backbone',
    'underscore',
    'libs/jquery-ui',
    'app',
    'modules/users/models/user',
    'base/views/item_view',
    'text!modules/users/templates/profile.html',
    'i18n!modules/users/nls/edit-profile',
    'modules/users/views/users_list',
    ], function ($, Backbone, _ , j_ui, App, UserModel , BaseItemView, ProfileTpl , labels, UsersListView) {
                        
        //jquery ui must be loaded first
        require(['libs/jqueryPlugins/inputosaurus']);      
                                    
        var UserProfile = BaseItemView.extend({
            attributes: {
                id : "profile"
            },
            events : {
                'click      .nav li'                 : 'changeContent',
                'click      .follow-user-profile'    : 'toggleFollow',
                'hover      .btn-hover'              : 'hoverBtn',
                'mouseout   .btn-hover'              : 'leaveBtn',
                'click     .post-page'               : 'newPost',
                'click     .suggest-user'            : 'suggestUser',
                'click      #save-new-suggest-user'  : 'sendSuggestions'
            },
            initialize : function(opt) {
                var self = this;
                this.model = new UserModel({
                    id : opt.user_id
                });
                this.model.fetch({
                    silent : true,
                    success : function(model){
                        self.render();        
                        
                        var tabIndex = self.options.tabIndex;
                        if(!tabIndex){
                            self.options.callee['beadsList'](model.get('id'));
                        }
                        else{
                            self.options.callee[tabIndex + 'List'](model.get('id'), model , (opt.status || null));
                        }
                    }
                });                  
                
                $('#main-container').html(this.$el);
                this.selectedId = [];
                this.selectedValue = [];
            },

            render : function() {
                this.template = _.template(ProfileTpl);
                var obj = _.extend(this.model.toJSON(), {
                    labels : labels
                });
                this.$el.append(this.template(obj));                             
                this.renderHeader()
                App.processScroll();         
                this.afterRender();
                
                return this;
            },
            afterRender : function()
            {
                var self = this;
                $("#user-suggestion-input").inputosaurus();
                
                $("#user-suggestion input").autocomplete({
                    source: function(request, response) {                        
                        $.ajax({
                            url  : "api/users",
                            dataType : 'JSON',
                            data : {
                                options : {
                                    'name' : $("#user-suggestion input").val() 
                                }
                            },
                            success: function(resp) {
                                var users = [];
                                var list = resp.list;
                                
                                _.each(list,function(user){
                                    var user = {
                                        label : user.first_name + ' ' + user.last_name,
                                        value : user.first_name + ' ' + user.last_name,
                                        id : user.id
                                   }
                                    users.push(user);
                                })
                                response(users, request.term );
                            }
                        });
                    },                    
                    minLength: 1
                });
                
                $("#user-suggestion input").on( "autocompleteselect", function(event, ui) {
                    self.selectedId.push(ui.item.id);
                    self.selectedValue.push(ui.item.value);
                });                  
            },
            renderHeader : function(){                
                this.headerTemplate = _.template(HeaderTpl);
                var obj = _.extend(this.model.toJSON(), {
                    labels : labels
                });
                
                $('#header-container', this.$el).empty();
                $('#header-container', this.$el).html(this.headerTemplate(obj));
                
                         
                this.beadsCollection =  new SitesCollection([],{
                    url : 'api/sites/user/' + this.model.get('id') + '/follows'
                }); 
                
                
                this.beadsCollection.fetch({
                    success : function(resp){                                
                     
                        for(var i = 0; i < 4; i++){
                            var tinySiteTemplate = _.template(tinySiteTpl); 
                            if(resp.models[i]) {
                                var obj =  resp.models[i].toJSON();
                                obj.exist = true;
                            } else{
                                var obj = {
                                    exist : false
                                };
                            }
                            var direction = ((i%2) == 0) ? 'left' : 'right';
                            obj.direction = direction;
//                            $('.beads-user-profile-two').append(tinySiteTemplate(obj));
                            $('.beads-user-profile-two [rel=tooltip]').tooltip();         
                        }
                    }
                });  

                var tabIndex = this.options.tabIndex || 'beads';
                $('a[data-type="'+ tabIndex +'"]').parent().addClass('active');
                
            },
            
            leaveBtn : function(e){
                $(e.target).removeClass('btn-warning');
                if(!this.model.get($(e.target).data('type'))){
                    $(e.target).removeClass('btn-primary');
                }                
            },
            
            hoverBtn : function(e){ 
                if(!$(e.target).data('type') || $(e.target).data('type') == ''){
                    return false;    
                } 
                if(this.model.get($(e.target).data('type'))){
                    $(e.target).addClass('btn-warning');
                }else{
                    $(e.target).addClass('btn-primary');
                }
            },
            
            changeContent : function(e , tab_index){
                if(tab_index){
                    $('.active').removeClass('active');
                    $('a[data-type="'+ tab_index +'"]').parent().addClass('active');
                }
                else{
                    var el = $(e.target);
                    $('.active').removeClass('active');
                    el.parent().addClass('active');
                    Backbone.history.navigate('profile/' + this.model.get('id')+ '/'+el.data('type'), {
                        trigger : true
                    });
                }
            },
            
            
        });

        return UserProfile;
    });
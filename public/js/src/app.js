define([
    'jquery',
    'bootstrap',
    'underscore',
    'backbone',
    'router',
    'modules/users/models/user',
    'base/views/base_view',
    'base/views/header',
    ] , function ($, bootstrap, _, Backbone, router, UserModel, BaseView, HeaderView) {
        var ajaxSetup = function(){
            $.ajaxSetup({
                beforeSend: function(jqXHR, settings){
                    $('.loading').modal()
                    $(".loading-image").show();
                },
                complete : function(){
                    setTimeout(function(){
                        $('.loading').modal('hide');
                        $(".loading-image").hide();
                    }, 1000)

                }
            });

        };

        var processScroll = function(){
            var $win = $(window)
            , $nav = $('#primary-nav')
            , navTop = $('#primary-nav').length && $('#primary-nav').offset().top - 74
            , isFixed = 1
            , isAddH = false;

            processScroll2();
            $win.unbind('scroll',processScroll2);
            $win.bind('scroll', processScroll2);

            function processScroll2() {
                var i, scrollTop = $win.scrollTop();

                if (scrollTop >= navTop && !isFixed) {
                    if(!isAddH){
                        //                        var h = $('#main-container').height();
                        //                        $('#main-container').height(h + 5);
                        isAddH = true;
                    }else if(isAddH){
                        //                        var h = $('#main-container').height();
                        //                        $('#main-container').height(h - 5);
                        isAddH = false;
                    }
                    isFixed = 1;
                    $('#primary-nav').addClass('subnav-fixed');
                }
                else if (scrollTop <= navTop && isFixed) {
                    isFixed = 0;
                    $('#main-container').css('height', 'none');
                    $('#primary-nav').removeClass('subnav-fixed');
                }
            }
        };

        var initialize = function() {

            window.currentUser = new UserModel();
            if(current_user){
                window.currentUser.set(window.current_user)
                window.currentUser.unset('password');
            }

            router.initialize();

            window.scrollTo(0, 0);

            $(window).bind('hashchange', function(){
                window.scrollTo(0, 0);
            });

        };

        return {
            initialize          : initialize,
            processScroll       : processScroll,
            ajaxSetup           : ajaxSetup,
            BaseView            : BaseView
        };
    });

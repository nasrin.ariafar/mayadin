require.config({
    baseUrl: "/js/src",
    waitSeconds: 60,
    paths: {
        jquery                      : 'vendor/jquery/jquery',
        underscore                  : 'vendor/underscore-amd/underscore',
        'underscore.string'         : 'vendor/underscore.string/lib/underscore.string',
        backbone                    : 'vendor/backbone-amd/backbone',
        'backbone.routefilter'      : 'vendor/backbone.routefilter/src/backbone.routefilter',
        'deep-model'                : 'vendor/backbone-deep-model/src/deep-model',
        bootstrap                   : 'vendor/bootstrap-3.0.2-dist/dist/js/bootstrap.min',
        text                        : 'vendor/requirejs-text/text',
        json                        : 'vendor/requirejs-plugins/src/json',
        i18n                        : 'vendor/requirejs-i18n/i18n',
        'contextmenu'               : 'vendor/sydcanem-bootstrap-contextmenu/bootstrap-contextmenu',
        "bootstrap_selectpicker"    : 'vendor/bootstrap-plugins/bootstrap-select.min',
        'pwt-date'                  : 'vendor/pwt.datepicker-master/js/pwt-date',
        'pwt-datepicker.min'        : 'vendor/pwt.datepicker-master/js/pwt-datepicker',
        "rtl-slider"                : 'vendor/jquery/jquery.ui.slider-rtl.min',
        "jquery-ui"                 : 'vendor/jquery/jquery-ui-1.10.3.custom.min',
        "jquery-helper"             : 'vendor/jquery/jquery-helper',
        "pageslide"                 : 'vendor/jquery/jquery.pageslide',
        "sider"                     : 'vendor/jquery.sidr',
        'jalali'                    : 'vendor/JalaliJSCalendar-master/jalali',
        'calendar'                  : 'vendor/JalaliJSCalendar-master/calendar',
        'calendar-setup'            : 'vendor/JalaliJSCalendar-master/calendar-setup',
        'calendar-fa'               : 'vendor/JalaliJSCalendar-master/lang/calendar-fa',
        'smartWizard'               : 'vendor/jQuery-Smart-Wizard-master/js/jquery.smartWizard',
        'map'                       : 'vendor/map/gmap3',
        'backbone.googlemaps'       : 'vendor/backbone.googlemaps-master/lib/backbone.googlemaps',
        'kwicks'                    : 'vendor/kwicks-master/jquery.kwicks',
        'chart'                     : 'vendor/Chart.js-master/Chart'


    },
    locale : 'fa-ir',
    shim: {
        bootstrap : {
            deps : ['jquery']
        },
        'pwt-date' : {
            deps : ['jquery']
        },
        'pwt-datepicker.min' :{
            deps :['jquery', 'pwt-date']
        },
        'rtl-slider': {
            deps: ['jquery','jquery-ui']
        },
        'underscore.string' : {
            deps : ['underscore']
        },
       
        'backbone.routefilter' : {
            deps : ['backbone', 'underscore']
        },
        'kwicks' : {
            deps : ['jquery']
        },
        
        'table-sorter' : {
            deps : ['jquery']
        },

        'table-sorter-widget' :{
            deps : ['jquery','table-sorter']
        },

        'pageslide' : {
            deps : ['jquery']
        },
        
        'bootstrap_selectpicker' : {
            deps: ['jquery','bootstrap']
        }

    }
});

require(['jquery','underscore', 'backbone','app', 'bootstrap', 'backbone.routefilter'], function ($,_, Backbone, App) {

    App.initialize();
    App.ajaxSetup();

    Events = _.extend({}, Backbone.Events);
    
    jQuery(document).ready(function(){
        require(window.__ModulesList, function() {
            Backbone.history.start();
            //Math Round
            var _round = Math.round;
            Math.round = function(number, decimals){
                if (arguments.length == 1)
                    return _round(number);

                var multiplier = Math.pow(10, decimals);
                return _round(number * multiplier) / multiplier;
            }
            $(window).scroll(function () {
                if ($(document).height() <= $(window).scrollTop() + $(window).height()) {
                    Events.trigger("scroll:end");
                }
            });
        });
    });
});

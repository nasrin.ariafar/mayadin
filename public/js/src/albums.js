define([
    'backbone',
    'modules/albums/router',
], function (Backbone, AlbumRouter) {
    var albumRouter = new AlbumRouter();
});

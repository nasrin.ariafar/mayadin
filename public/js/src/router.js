define([
    'jquery',
    'underscore',
    'backbone',
    'base/views/header',
    'frontend/collections/items',
    'frontend/models/item',
    'frontend/views/login',
    'frontend/views/items_list',
    'frontend/views/news_list',
    'frontend/views/markets_list',
    'frontend/views/links_list',
    'frontend/views/questions_answers_list',
    'frontend/views/images_list',
    'frontend/views/evaluation_form',
    'frontend/views/user_form',
    'frontend/views/station_form',
    'frontend/views/item_form',
    'frontend/views/link_form',
    'frontend/views/prices_list',
    'frontend/views/evaluation_preview',
    'modules/images/views/config',
    'frontend/collections/activities',
    'frontend/views/activities_list',
    ], function ($, _, Backbone,HeaderView, ItemsCollection, ItemModel, LoginView,
                ListView, NewsList, MarketsList, LinksList, QuestionsAswersList,
                ImagesListView, EvaluationForm, UserForm, StationForm, ItemForm, LinkForm,
                PricesListView,ItemPreview, config,
                ActivitiesCollection, ActivitiesListView){
        var AppRouter = Backbone.Router.extend({
            routes : {
                ''                                      : 'homePage',
                'news'                                  : 'showNews',
                'news/:id'                              : 'addNews',
                'add/news'                              : 'addNews',

                'photoCoverage'                         : 'showNews',
                'photoCoverage/:id'                     : 'addPhotoCoverage',
                'add/photoCoverage'                     : 'addPhotoCoverage',

                'questionsAnswers'                      : 'showQuestionsAnswers',
                'add/questionAnswer'                    : 'addQuestionAsnwer',
                'questionsAnswers/:id'                  : 'addQuestionAsnwer',


                'suggestions-feedbacks'                 : 'showQuestionsAnswers',
                'add/suggestion-feedback'               : 'addQuestionAsnwer',

                'rules'                                 : 'showLinks',
                'add/rule?category=:category'           : 'addRuleByCategory',

                'markets'                               : 'showMarkets',
                'educations'                            : 'showLinks',
                'pricesList'                            : 'pricesList',



                

                'evaluations/poll'      : 'showEvaluationsList',
                'evaluations/exam'      : 'showEvaluationsList',
                'evaluations/contest'   : 'showEvaluationsList',
                'evaluation/:id/preview' : 'previewEvaluation',

                'add/poll'              : 'addEvaluation',
                'add/exam'              : 'addEvaluation',
                'add/contest'           : 'addEvaluation',
                'evaluations/:id'       : 'addEvaluation',
                
                'educations/:id'        : 'addEducations',
                'add/educations'        : 'addEducations',

                'stations'              : 'showList',
                'add/stations'          : 'addStations',
                'stations/:id'          : 'addStations',


                'links/magazines'       : 'showLinks',
                'links/papers'          : 'showLinks',
                'links/rules'           : 'showLinks',
                'add/magazines'         : 'addLink',
                'add/papers'            : 'addLink',
                'add/rules'             : 'addLink',
                'links/:id'             : 'addLink',



                'reports'               : 'showList',
                'changePassword'        : 'changePassword' ,

                '*type/:id/activities'  : 'showLogs'

            },
            
            before : function(){
                $(".navbar-nav .selected").removeClass("selected");
                
                if(!window.currentUser.get("id")){
                    $("body").addClass("before-login");
                    $("header").hide();
                }else{
                    $("body").removeClass("before-login");
                    $("header").show();
                    if(!this.header){
                        this.header = new HeaderView();
                    }
                    else{
                        this.header.render();
                        this.header.delegateEvents();
                    }
                }

                var menu_item = $('.navbar-nav a[href="#'+ Backbone.history.fragment +'"]');
                if(menu_item){
                    !menu_item.is(":hidden") &&  menu_item.addClass("selected");
                    menu_item.is(":hidden") &&  $('.navbar-nav a[href="#'+ Backbone.history.fragment +'"]').parents(".dropdown").addClass("selected")
                }

                $("#main-container").empty();
            },

            login : function(){
                loginView = new LoginView();
                $("#main-container").html(loginView.render().$el);
            },
            
            homePage : function() {
                if(window.currentUser.get("id"))
                    Backbone.history.navigate("news");

                else
                    this.login();
            },

            showEvaluationsList : function(){
                if(!window.currentUser.get("id"))
                    return;

                var  type = Backbone.history.fragment.split("/")[1];

                switch(type){
                    case "poll" :
                        var evaluation_type = 1;
                        break;
                    case "contest" :
                        var evaluation_type = 2;
                        break;
                    case "exam" :
                        var evaluation_type = 3;
                        break;
                }
                

                var collection =  new ItemsCollection([], {
                    target : 'evaluations',
                    model   : require('frontend/models/item')
                });
                collection.filters.set("type", evaluation_type,{
                    silent : true,
                    deleted : 0
                });
                
                var listView = new ListView({
                    target        : "evaluations",
                    type          : type,
                    collection  :collection
                })

                $("#main-container").html(listView.render().$el);
            },

            showPhotos : function(id){
                if(!window.currentUser.get("id"))
                    return;

                var collection = new ItemsCollection([], {
                    target      : "resources",
                    model   : require('frontend/models/item')
                });

                collection.filters.set({
                    parentType : "gallery"
                },{
                    silent : true
                })
                
                var listView = new ImagesListView({
                    target        : "resources",
                    collection  : collection,
                })

                $("#main-container").html(listView.render().$el);
                listView.registerEvents();
            },
            
            showQuestionsAnswers : function(){
                if(!window.currentUser.get("id"))
                    return;
                
                var target = "questionsAnswers",
                type =(Backbone.history.fragment == "questionsAnswers") ? 1 : 2,
                collection = new ItemsCollection([], {
                    target   : target,
                    model   : require('frontend/models/item')
                });

                collection.filters.set("type", type, {
                    silent : true
                });

                var listView = new QuestionsAswersList({
                    target      :  target,
                    collection  : collection
                });
                
                $("#main-container").html(listView.render().$el);
            },
            
            showList : function(type){
                if(!window.currentUser.get("id"))
                    return;
                
                var type = Backbone.history.fragment;
                var listView = new ListView({
                    target        : type,
                    collection  : new ItemsCollection([], {
                        target : type,
                        model   : require('frontend/models/item')
                    })
                })

                $("#main-container").html(listView.render().$el);
            },

            showNews : function(){
                if(!window.currentUser.get("id"))
                    return;

                var type = Backbone.history.fragment;
                var listView = new NewsList({
                    target        : type,
                    type          : type,
                    collection  : new ItemsCollection([], {
                        target : type,
                        model   : require('frontend/models/item')
                    })
                })

                $("#main-container").html(listView.render().$el);
            },

            showMarkets : function(){
                if(!window.currentUser.get("id"))
                    return;

                var type = Backbone.history.fragment,
                collection = new ItemsCollection([], {
                    target : type,
                    model   : require('frontend/models/market')
                });

                collection.filters.set("mrktParent", -1, {
                    silent : true
                });
                
                var listView = new MarketsList({
                    target        : type,
                    type          : type,
                    collection    : collection
                })

                $("#main-container").html(listView.render().$el);
            },

            showLinks : function(){
                if(!window.currentUser.get("id"))
                    return;

                var collection  = new ItemsCollection([], {
                    target  : "links",
                    model   : require('frontend/models/item')
                });

                collection.filters.set({
                    type        : ( Backbone.history.fragment == "rules" ) ? 1 : 2,
                    category    : ( Backbone.history.fragment == "rules" ) ? 1 : 5,
                    parentId    : 0

                },{
                    silent : true
                });
                
                var listView = new LinksList({
                    target          : "links",
                    type            : Backbone.history.fragment,
                    collection      : collection
                })

                $("#main-container").html(listView.render().$el);
            },

            showUsersList : function(type, id){
                if(!window.currentUser.get("id"))
                    return;
                
                var typeValue = (type == "managers") ? 1 : 0;
                var collection =  new ItemsCollection([], {
                    target : 'humans',
                    model   : require('frontend/models/item')
                });
                collection.filters.set("type", typeValue,{
                    silent : true
                });

                var listView = new ListView({
                    target        : "humans",
                    type        : type,
                    collection  :collection
                })

                $("#main-container").html(listView.render().$el);
            },
            addEducations : function(id){
                if(!window.currentUser.get("id"))
                    return;
                
                var type =  "educations";//Backbone.history.fragment.split("/")[1];
                var model = new ItemModel({
                    target : type,
                    id     : id
                });

                this.newItem(type , model);
            },

            addOffice : function(id){
                if(!window.currentUser.get("id"))
                    return;

                var type = "offices";//Backbone.history.fragment.split("/")[1];

                var model = new ItemModel({
                    target : type,
                    id     : id
                });

                this.newItem(type , model);

            },
            
            addNews : function(id){
                if(!window.currentUser.get("id"))
                    return;
               
                var model = new ItemModel({
                    target  : "news",
                    id      : id
                });

                this.newItem("news" , model);
            },

            addPhotoCoverage : function(id){
                if(!window.currentUser.get("id"))
                    return;

                var model = new ItemModel({
                    target  : "photoCoverage",
                    id      : id
                });
                this.newItem("photoCoverage" , model);
            },

            addQuestionAsnwer : function(id){
                if(!window.currentUser.get("id"))
                    return;

                var model = new ItemModel({
                    target  : "questionsAnswers",
                    type    : (Backbone.history.fragment == "add/questionAnswer") ? 1 : 2,
                    id      : id
                });
                this.newItem("questionsAnswers" , model);
            },

            addMemory : function(id){
                if(!window.currentUser.get("id"))
                    return;

                var type = "memories";//Backbone.history.fragment.split("/")[1];

                var model = new ItemModel({
                    target : type,
                    id     : id
                });

                this.newItem(type , model);

            },


            addSurvivor : function(id){
                if(!window.currentUser.get("id"))
                    return;
                
                var type = "news";

                var model = new ItemModel({
                    target : type,
                    isSurvivor : 1,
                    id     : id
                });

                this.newItem(type , model)
            },

            addStations : function(id){
                if(!window.currentUser.get("id"))
                    return;

                var type = Backbone.history.fragment.split("/")[1];

                if(Backbone.history.fragment.split("/")[0] == "stations"){
                    id = type;
                    type = null;
                }

                var model = new ItemModel({
                    target : "stations",
                    id     : id
                });
                var form = new StationForm({
                    type    :type,
                    model   : model
                });
            },

            addUser : function(id){
                if(!window.currentUser.get("id"))
                    return;
                
                var type = Backbone.history.fragment.split("/")[1];

                if(Backbone.history.fragment.split("/")[0] == "humans"){
                    id = type;
                    type = null;
                }

                var model = new ItemModel({
                    target : "humans",
                    type : (type == "managers") ? 1 : 0,
                    id     : id
                });
                var form = new UserForm({
                    type :type,
                    model    : model
                });
            },

            addLink : function(id){
                if(!window.currentUser.get("id"))
                    return;

                var type = Backbone.history.fragment.split("/")[1];

                if(Backbone.history.fragment.split("/")[0] == "links"){
                    id = type;
                    type = null;
                }
                var val = 0;
                
                switch(type){
                    case "papers" :
                        val = 1;
                        break;

                    case "rules" :
                        val = 3;
                        break;
                }

                var model = new ItemModel({
                    target : "links",
                    type :  val,
                    id     : id
                });
                var form = new ItemForm({
                    type    :type,
                    model   : model
                });
            },

            addRuleByCategory : function(category){
                if(!window.currentUser.get("id"))
                    return;

                var form = new LinkForm({
                    type : 1,
                    model : new ItemModel({
                        target      : "links",
                        category    : category,
                        type        :  1
                    })
                });
            },
            
            newItem : function(type, model){
                if(!window.currentUser.get("id"))
                    return;
                
                var form = new ItemForm({
                    type : type,
                    model : model
                })
            },

            addEvaluation : function(id){
                if(!window.currentUser.get("id"))
                    return;

                var model = new ItemModel({
                    target : "evaluations",
                    id     : id
                });

                var type = Backbone.history.fragment.split("/")[1];
                var form = new EvaluationForm({
                    type :type,
                    id  : id,
                    model : model
                });
                
            },
            
            previewEvaluation : function(id){
                if(!window.currentUser.get("id"))
                    return;

                var view = new ItemPreview({
                    evalutionId  : id
                });

                $("#main-container").html(view.$el);
            },

            changePassword : function(){
                if(!window.currentUser.get('id')){
                    Backbone.history.navigate('', true);
                    return;
                }
                else{
                    require(['modules/users/views/edit_profile'], function(EditProfileView) {
                        var editProfile = new EditProfileView({
                            type    : 'changePassword'
                        });
                        $("#main-container").html(editProfile.$el);
                        editProfile.registerEvents();
                    });
                }
            },

            pricesList : function(){
                var pricesList = new PricesListView();
                $("#main-container").html(pricesList.render().$el);
                pricesList.registerEvents();
            },

            showLogs : function(rout, itemId){
                if(!window.currentUser.get("id"))
                    return;

                var collection =  new ActivitiesCollection([]);
                collection.filters.set("itemId", itemId,{
                    silent : true
                });

                var listView = new ActivitiesListView({
                    collection  :collection
                })

                $("#main-container").html(listView.render().$el);
            }

        });

        var initialize = function() {
            var app_router = new AppRouter();

        };

        return {

            initialize : initialize
        };
    });

<?php

class Tea_Test_ControllerTestCase extends Zend_Test_PHPUnit_ControllerTestCase
    {

    public function setUp()
        {
        $this->appConfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
        $this->bootstrap = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini');
        Zend_Registry::set('bootstrap', $this->bootstrap->getBootstrap());
        parent::setUp();
        }

    public function createTestUser()
    {
        $userService = Users_Service_User::getInstance();
        
        $user = new Users_Model_User;
        $user->setCorpId('');
        $user->setDeptId('');
        $user->setFirstName('first_name');
        $user->setLastName('last_name');
        $user->setEmail(TEST_USER_EMAIL);
        $user->setUsername('test');
        $user->setPassword(TEST_USER_PASS);
        $user->setGender(Users_Model_User::GENDER_MALE);
        $user->setTimezone('timezone');
        $user->setLocale('locale');
        $user->setCreatedById('');
        $user->setUpdatedById('');
        $user = $userService->save($user);
        
        return $user;
    }
    
    public function createTestUserProfile(Users_Model_User $user)
    {
        $userProfileService = Users_Service_Profile::getInstance();
        
        $userProfile = new Users_Model_Profile();
        $userProfile->setCountry('Iran');
        $userProfile->setCreatedById($user->getId());
        $userProfile->setUpdatedById($user->getId());
        $userProfile = $userProfileService->save($userProfile);
        
        return $userProfile;
    }
    
    public function createTestCorporation(Users_Model_User $user)
    {
        $corporationService = Admins_Service_Corporation::getInstance();
        
        $corp = new Admins_Model_Corporation();
        $corp->setTitle('test corporation');
        $corp->setUrl('test@corporation.com');
        $corp->setAbout('this is for test');
        $corp->setCreatedById($user->getId());
        $corp->setUpdatedById($user->getId());
        $corp->setNew(true);
        $corp = $corporationService->save($corp);
        
        return $corp;
    }
    
    public function createTestDepartment(Users_Model_User $user, Admins_Model_Corporation $corp)
    {
        $departmentService = Admins_Service_Department::getInstance();
        
        $dept = new Admins_Model_Department();
        $dept->setCorpId($corp->getId());
        $dept->setPath('');
        $dept->setTitle('test department');
        $dept->setUrl('test@department.com');
        $dept->setHasAddress(Admins_Model_Department::HAS_ADDRESS_NO);
        $dept->setCreatedById($user->getId());
        $dept->setUpdatedById($user->getId());
        $dept->setNew(true);
        $dept = $departmentService->save($dept);
        
        return $dept;
        
    }
        
    public function createTestPost(Users_Model_User $user)
        {
        $post = new Posts_Model_Post();
        $post->setTitle('test title');
        $post->setBody('test body');
        $post->setType('1');
        $post->setPosterId($user->getId());
        $post->setCreatedById($user->getId());
                $post->setUpdatedById($user->getId());

        $postService = Posts_Service_Post::getInstance();
        $post = $postService->save($post);
        return $post;
        }

    public function createTestComment(Posts_Model_Post $post, Users_Model_User $user)
        {
        $comment = new Posts_Model_Comment();
        $comment->setMessage('testing message');
        $comment->setAuthorId($user->getId());
        $comment->setPostId($post->getId());
        $commentService = Posts_Service_Comment::getInstance();
        $comment = $commentService->save($comment);
        return $comment;
        }

    public function createTestLike(Posts_Model_Comment $comment, Users_Model_User $user)
        {
        $like = new Posts_Model_CommentLike();
        $like->setCommentId($comment->getId());

        $like->setLikerId($user->getId());
        $likeService = Posts_Service_CommentLike::getInstance();
        $like = $likeService->save($like);
        return $like;
        }

    public function createTestProject(Users_Model_User $admin)
        {
        $projectService = Projects_Service_Project::getInstance();

        $project = new Projects_Model_Project();
        $project->setTitle('test title');
        $project->setDescription('test description');
        $project->setCreatedById($admin->getId());
        $project->setUpdatedById($admin->getId());
        $project = $projectService->save($project);

        return $project;
        }

    public function createTestLabel(Users_Model_User $admin, Projects_Model_Project $project)
        {
        $labelService = Projects_Service_Label::getInstance();

        $label = new Projects_Model_Label();
        $label->setProjectId($project->getId());
        $label->setTitle('test label');
        $label->setColor(Projects_Model_Label::COLOR_RED);
        $label->setCreatedById($admin->getId());
        $label->setUpdatedById($admin->getId());
        $label = $labelService->save($label);

        return $label;
        }

    public function createTestCustom(Users_Model_User $admin, Projects_Model_Project $project)
        {
        $customService = Projects_Service_Custom::getInstance();

        $custom = new Projects_Model_Custom();
        $custom->setProjectId($project->getId());
        $custom->setTitle('test custom');
        $custom->setType(Projects_Model_Custom::TYPE_FLOAT);
        $custom->setOptions('.');
        $custom->setValue('1');
        $custom->setAssigneeId($admin->getId());
        $custom->setOrder(11);
        $custom->setCreatedById($admin->getId());
        $custom->setUpdatedById($admin->getId());
        $custom = $customService->save($custom);

        return $custom;
        }

    public function createTestMembership(Users_Model_User $admin, Projects_Model_Project $project)
        {
        $membershipService = Projects_Service_Membership::getInstance();

        $membership = new Projects_Model_Membership();
        $membership->setProjectId($project->getId());
        $membership->setTitle('test membership');
        $membership->setCreatedById($admin->getId());
        $membership->setUpdatedById($admin->getId());
        $membership = $membershipService->save($membership);

        return $membership;
        }

    public function createTestMember(Users_Model_User $admin, Projects_Model_Project $project, Projects_Model_Membership $membership)
        {
        $memberService = Projects_Service_Member::getInstance();

        $member = new Projects_Model_Member();
        $member->setProjectId($project->getId());
        $member->setMembershipId($membership->getId());
        $member->setUserId($admin->getId());
        $member->setRoleId('role');
        $member->setCreatedById($admin->getId());
        $member->setUpdatedById($admin->getId());
        $member->setNew(true);
        $member = $memberService->save($member);

        return $member;
        }

    public function createTestStage(Users_Model_User $admin, Projects_Model_Project $project)
        {
        $stageService = Projects_Service_Stage::getInstance();

        $stage = new Projects_Model_Stage();
        $stage->setProjectId($project->getId());
        $stage->setTitle('test stage');
        $stage->setCreatedById($admin->getId());
        $stage->setUpdatedById($admin->getId());
        $stage = $stageService->save($stage);

        return $stage;
        }

    public function createTestTodoList(Users_Model_User $admin, Projects_Model_Project $project)
        {
        $todoListService = Projects_Service_TodoList::getInstance();

        $todoList = new Projects_Model_TodoList();
        $todoList->setProjectId($project->getId());
        $todoList->setTitle('test todolist');
        $todoList->setCreatedById($admin->getId());
        $todoList->setUpdatedById($admin->getId());
        $todoList = $todoListService->save($todoList);

        return $todoList;
        }

    public function createTestTodo(Users_Model_User $admin, Projects_Model_Project $project, Projects_Model_Stage $stage)
        {
        $todoService = Projects_Service_Todo::getInstance();

        $todo = new Projects_Model_Todo();
        $todo->setProjectId($project->getId());
        $todo->setStageId($stage->getId());
        $todo->setTitle('test todo');
        $todo->setCreatedById($admin->getId());
        $todo->setUpdatedById($admin->getId());
        $todo->setNew(true);
        $todo = $todoService->save($todo);

        return $todo;
        }

    public function createTestChecklist(Users_Model_User $admin, Projects_Model_Project $project, Projects_Model_Stage $stage, Projects_Model_Todo $todo)
        {
        $checklistService = Projects_Service_Checklist::getInstance();

        $checklist = new Projects_Model_Checklist();
        $checklist->setProjectId($project->getId());
        $checklist->setStageId($stage->getId());
        $checklist->setTodoId($todo->getId());
        $checklist->setTitle('test checklist');
        $checklist->setCreatedById($admin->getId());
        $checklist->setUpdatedById($admin->getId());
        $checklist->setNew(true);
        $checklist = $checklistService->save($checklist);

        return $checklist;
        }

    public function createTestTodoLabel(Projects_Model_Project $project, Projects_Model_Todo $todo)
        {
        $todoLabelService = Projects_Service_TodoLabel::getInstance();

        $todoLabel = new Projects_Model_TodoLabel();
        $todoLabel->setTodoId($todo->getId());
        $todoLabel->setProjectId($project->getId());
        $todoLabel->setNew(true);
        $todoLabel = $todoLabelService->save($todoLabel);

        return $todoLabel;
        }

    public function createTestCalendar(Users_Model_User $admin)
        {
        $calendarService = Calendars_Service_Calendar::getInstance();

        $calendar = new Calendars_Model_Calendar();
        $calendar->setName('test calendar');
        $calendar->setColor('00000000');
        $calendar->setCreatedById($admin->getId());
        $calendar->setUpdatedById($admin->getId());
        $calendar = $calendarService->save($calendar);

        return $calendar;
        }

    public function createTestActivity(Users_Model_User $actor)
        {
        $activityService = Feeds_Service_Activity::getInstance();

        $activity = new Feeds_Model_Activity();
        $activity->setActorId($actor->getId());
        $activity->setAction(0);
        $activity = $activityService->save($activity);

        return $activity;
        }

    public function createTestActivityComment(Users_Model_User $author, Feeds_Model_Activity $activity)
        {
        $activityCommentService = Feeds_Service_ActivityComment::getInstance();

        $activityComment = new Feeds_Model_ActivityComment();
        $activityComment->setActivityId($activity->getId());
        $activityComment->setAuthorId($author->getId());
        $activityComment->setMessage('test comment');
        $activityComment = $activityCommentService->save($activityComment);

        return $activityComment;
        }

    public function createTestActivityCommentLike(Users_Model_User $liker, Feeds_Model_Activity $activity, Feeds_Model_ActivityComment $activityComment)
        {
        $activityCommentLikeService = Feeds_Service_ActivityCommentLike::getInstance();

        $activityCommentLike = new Feeds_Model_ActivityCommentLike();
        $activityCommentLike->setCommentId($activityComment->getId());
        $activityCommentLike->setLikerId($liker->getId());
        $activityCommentLike->setActivityId($activity->getId());
        $activityCommentLike = $activityCommentLikeService->save($activityCommentLike);

        return $activityCommentLike;
        }

    public function createTestFeed(Users_Model_User $user, Feeds_Model_Activity $activity)
        {
        $feedService = Feeds_Service_Feed::getInstance();

        $feed = new Feeds_Model_Feed();
        $feed->setUserId($user->getId());
        $feed->setActivityId($activity->getId());
        $feed->setCreatedById($user->getId());
        $feed->setUpdatedById($user->getId());

        $feed = $feedService->save($feed);

        return $feed;
        }

    protected function cleanupTable($table)
        {
        switch ($table)
            {
            case 'users':
                Users_Service_User::getInstance()->removeAll();
                break;

            case 'projects':
                Projects_Service_Project::getInstance()->removeAll();
                break;

            case 'project_memberships':
                Projects_Service_Membership::getInstance()->removeAll();
                break;

            case 'project_members':
                Projects_Service_Member::getInstance()->removeAll();
                break;

            case 'calendars':
                Calendars_Service_Calendar::getInstance()->removeAll();
                break;

            case 'calendar_events':
                Calendars_Service_Event::getInstance()->removeAll();
                break;

            case 'posts':
                Posts_Service_Post::getInstance()->removeAll();
                break;

            case 'likes':
                Posts_Service_CommentLike::getInstance()->removeAll();
                break;

            case 'comments':
                Posts_Service_Comment::getInstance()->removeAll();
                break;
            
            case 'follows':
                Posts_Service_Follow::getInstance()->removeAll();
                break;
        
            case 'shares':
                Posts_Service_Share::getInstance()->removeAll();
                break;
            
            case 'votes':
                Posts_Service_Vote::getInstance()->removeAll();
                break;
            
            case 'reminders':
                Reminders_Service_Reminder::getInstance()->removeAll();
                break;
              
    
            case 'polls':
                Polls_Service_Poll::getInstance()->removeAll();
                break;
            
            case 'poll_votes':
                Polls_Service_Voting::getInstance()->removeAll();
                break;
            
            case 'poll_voting_items':
                Polls_Service_VotingItem::getInstance()->removeAll();
                break;
            
            case 'poll_assignees':
                Polls_Service_Assignee::getInstance()->removeAll();
                break;
            
            case 'poll_voting_responses':
                Polls_Service_VotingResponse::getInstance()->removeAll();
                break;
            
            
            
            }
        }

    protected function createDataFromXml($xmlFile)
        {
        if (!is_readable($xmlFile))
            {
            $this->markTestSkipped(
                    'The "' . $xmlFile . '" is not available.'
            );
            return false;
            }
        $xmlObj = simplexml_load_file($xmlFile);
        $conn = Tea_Db_Connection::factory()->getConnection();

        foreach ($xmlObj as $collection)
            {
            $name = (string) $collection['name'];
            $clean = (string) $collection['cleanup'];
            if (strtolower($clean) == 'true')
                {
                $conn->{$name}->drop();
                }
            foreach ($collection as $row)
                {
                $record = array();
                foreach ($row as $key => $item)
                    {

                    if (isset($item->row))
                        {
                        $value = array();
                        foreach ($item->row as $child)
                            {
                            $_val = array();
                            foreach ($child as $k => $v)
                                {
                                $k = (string) $k;
                                $v = (string) $v;
                                if (isset($k['type']))
                                    {
                                    switch ($item['type'])
                                        {
                                        case 'int':
                                        case 'integer':
                                            $v = (int) $v;
                                            break;
                                        case 'date':
                                        case 'datetime':
                                            $v = strtotime($v);
                                            break;
                                        }
                                    }
                                $_val[$k] = $v;
                                }

                            $value[] = $_val;
                            }
                        } else
                        {
                        $value = (string) $item;
                        if (isset($item['type']))
                            {
                            switch ((int) $item['type'])
                                {
                                case 'int':
                                case 'integer':
                                    $value = (int) $value;
                                    var_dump($value);
                                    break;
                                case 'date':
                                case 'datetime':
                                    $value = strtotime($value);
                                    break;
                                }
                            }
                        }

                    if (substr($key, -5) == '_date'
                            && is_string($value)
                    )
                        {
                        $value = strtotime($value);
                        }
                    $record[$key] = $value;
                    }

                $record['_id'] = new MongoId($record['id']);
                $conn->{$name}->insert($record);
                }
            }
        }

    }


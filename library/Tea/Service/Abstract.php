<?php

abstract class Tea_Service_Abstract
{
    protected function explodeKey($key)
    {
        $propArray = explode('.', $key);
        $prefix    = '';
        $property  = '';

        if (count($propArray) > 1) {
            $prefix   = $propArray[0];
            $property = $propArray[1];
        } elseif (count($propArray) > 0) {
            $prefix   = '';
            $property = $propArray[0];
        }

        return array(
            'prefix'   => $prefix,
            'property' => $property
        );
    }

    protected function getFuzzyFormat($datetime)
    {
        include_once 'Date_HumanDiff/HumanDiff.php';
        $dh = new Date_HumanDiff();
        return $dh->get($datetime);
    }

    protected function staticGet($key)
    {
        if (!Zend_Registry::isRegistered('cache')) {
            return false;
        }

        $cacheId = $this->getCacheKey($key);


        if ($ret = Zend_Registry::get('cache')->load($cacheId)) {
            return $ret;
        }

        return false;
    }

    protected function staticSet($key, $obj)
    {
        if (!Zend_Registry::isRegistered('cache')) {
            return false;
        }

        $cacheId = $this->getCacheKey($key);
        return Zend_Registry::get('cache')->save($obj, $cacheId);
    }

    protected function staticDel($key)
    {
        if (!Zend_Registry::isRegistered('cache')) {
            return false;
        }

        $cacheId = $this->getCacheKey($key);
        return Zend_Registry::get('cache')->remove($cacheId);
    }

    private function getCacheKey($vals)
    {
        $key = get_class($this) . ':';

        if (is_array($vals)) {
            sort($vals);
            $vals  = array_values($vals);
            $keys  = array_keys($vals);
            $key  .= implode(',', $vals);
            $key  .= implode(',', $keys);
        } elseif (is_object($vals)) {
            throw Exception('Object in param');
        } else {
            $key .= (string)$vals;
        }

        return sha1($key);
    }
}


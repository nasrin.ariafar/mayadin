<?php

class Feeds_ApiControllerTest extends Tea_Test_ControllerTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testIndexAction()
    {
        $this->cleanupTable('users');
        $this->cleanupTable('activities');
        $this->cleanupTable('activity_comments');
        $this->cleanupTable('activity_comment_likes');
        $this->cleanupTable('feeds');
        
        $this->createTestUser();
        
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => TEST_USER_EMAIL,
                    'password' => TEST_USER_PASS
                )
            );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);

        $this->resetRequest();
        $this->resetResponse();
        $this->dispatch('/api/feeds');
        $this->assertResponseCode(200);
        $this->assertModule('feeds');
        $this->assertController('Api');
        $this->assertAction('index');
    }
}


<?php

class Feeds_Service_ActivityCommentTest extends Tea_Test_ControllerTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->cleanupTable('users');
        $this->cleanupTable('activities');
        $this->cleanupTable('activity_comments');
    }

    public function tearDown()
    {
    }

    public function testGetInstance()
    {
        $instance = Feeds_Service_ActivityComment::getInstance();
        $this->assertInstanceOf('Feeds_Service_ActivityComment', $instance);
    }

    public function testCreateActivityComment()
    {
        /*
         * activity is created
         * a comment is created for this activity
         * 
         * comment retrieves by id, 
         * and it's properties compare with original comment
         */
        
        $activityCommentService = Feeds_Service_ActivityComment::getInstance();
        
        $user       = $this->createTestUser();
        $activity   = $this->createTestActivity($user);
        
        $activityComment = new Feeds_Model_ActivityComment();
        $activityComment->setActivityId($activity->getId());
        $activityComment->setAuthorId($user->getId());
        $activityComment->setMessage('test comment');
        $activityComment->setNew(true);
        $activityComment = $activityCommentService->save($activityComment);
        
        $testActivityComment = $activityCommentService->getByPK($activityComment->getId());
        $this->assertEquals($testActivityComment->getActivityId(), $activity->getId());
        $this->assertEquals($testActivityComment->getAuthorId(), $user->getId());
        $this->assertEquals($testActivityComment->getMessage(), 'test comment');
    }    

    public function testDeleteActivityComment()
    {
        /*
         * activity is created
         * a comment is created for this activity
         * 
         * comment is deleted
         * 
         * comment retrieves by id, 
         * and result of this search should be null
         */

        $activityCommentService = Feeds_Service_ActivityComment::getInstance();
        
        $user               = $this->createTestUser();
        $activity           = $this->createTestActivity($user);
        $activityComment    = $this->createTestActivityComment($user, $activity);
        
        $activityCommentService->remove($activityComment);
        $testActivityComment = $activityCommentService->getByPK($activityComment->getId());
        $this->assertEquals($testActivityComment, null);
    }
}

<?php

class Posts_ApiControllerTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testIndexAction()
    {
        $this->cleanupTable('likes');
        $this->cleanupTable('comments');
        $this->cleanupTable('follows');
        $this->cleanupTable('shares');
        $this->cleanupTable('votes');
        $this->cleanupTable('posts');
        $this->cleanupTable('users');

        $user = $this->createTestUser();

        $this->dispatch('/api/posts/');
        $this->assertResponseCode(200);
        $this->assertModule('posts');
        $this->assertController('Api');
        $this->assertAction('index');
    }

    public function testPostAction()
    {
        $this->cleanupTable('posts');

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => 'test@gando.com',
                    'password' => 'test'
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'poster_id' => Users_Service_Auth::getInstance()->getCurrentUser()->getId(),
                    'type'      => 1,
                    'title'     => 'testing',
                    'body'      => 'testing body'
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('api/posts');
        $this->assertResponseCode(200);
        $result     = $this->response->getBody();
        $postArray  = json_decode($result, true);
        $this->assertEquals($postArray['poster_id'], Users_Service_Auth::getInstance()->getCurrentUser()->getId());
        $this->assertEquals($postArray['type'], 1);
        $this->assertEquals($postArray['title'], 'testing');
        $this->assertEquals($postArray['body'], 'testing body');

        $this->resetRequest();
        $this->resetResponse();
        $this->dispatch('/api/posts');
        $this->assertResponseCode(200);
        $this->assertModule('posts');
        $this->assertController('Api');
        $this->assertAction('index');
        $body   = $this->response->getBody();
        $this->assertNotEmpty($body);
        $result = json_decode($this->response->getBody(), true);
        $this->assertEquals($result['count'], 1);

        return $postArray['id'];
    }

    /**
     * @depends testPostAction
     */
    public function testGetAction($id)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('Get');
        $this->dispatch('/api/posts/' . $id);
        $this->assertResponseCode(200);
        $this->assertModule('posts');
        $this->assertController('Api');
        $this->assertAction('get');
        $body   = $this->response->getBody();
        $this->assertNotEmpty($body);
        $result = json_decode($body, true);
        $this->assertEquals($result['id'], $id);
        return $id;
    }

    /**
     * @depends testGetAction
     */
    public function testPutAction($id)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => TEST_USER_EMAIL,
                    'password' => TEST_USER_PASS
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);


        $this->request->setMethod('PUT')
            ->setPost(
                array(
                    'title' => 'modified title',
                    'body'  => 'modified body'
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('api/posts/' . $id);
        $this->assertResponseCode(200);

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('GET');
        $this->dispatch('/api/posts/' . $id);
        $this->assertResponseCode(200);
        $this->assertModule('posts');
        $this->assertController('Api');
        $this->assertAction('get');

        $body = $this->response->getBody();
        $this->assertNotEmpty($body);
        $post = json_decode($body, true);

        $this->assertEquals($post['title'], 'modified title');
        $this->assertEquals($post['body'], 'modified body');

        return $id;
    }

    /**
     * @depends testPutAction
     */
    public function testDeleteAction($id)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => TEST_USER_EMAIL,
                    'password' => TEST_USER_PASS
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('DELETE');
        $this->dispatch('/api/posts/' . $id);
        $this->assertResponseCode(200);
        $this->assertModule('posts');
        $this->assertController('Api');
        $this->assertAction('delete');

        $body = $this->response->getBody();
        $this->assertNotEmpty($body);
        $this->assertEquals($body, 'Success');

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('GET');
        $this->dispatch('/api/posts/' . $id);
        $this->assertResponseCode(404);
        $this->assertModule('posts');
        $this->assertController('Api');
        $this->assertAction('get');

        $body = $this->response->getBody();
        $this->assertNotEmpty($body);
        $this->assertEquals($body, 'Post Not Found');
    }

}
<?php

class Posts_Service_FollowTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->cleanupTable('likes');
        $this->cleanupTable('comments');
        $this->cleanupTable('follows');
        $this->cleanupTable('shares');
        $this->cleanupTable('votes');
        $this->cleanupTable('posts');
        $this->cleanupTable('users');
    }

    public function testGetInstance()
    {
        $instance = Posts_Service_Follow::getInstance();
        $this->assertInstanceOf('Posts_Service_Follow', $instance);
    }

    public function testCreateFollow()
    {
        $followService = Posts_Service_Follow::getInstance();
        $follow        = new Posts_Model_Follow();
        $user          = $this->createTestUser();
        $post          = $this->createTestPost($user);
        $follow->setPostId($post->getId());
        $follow->setFollowerId($user->getId());
        $follow->setNew(true);
        $follow        = $followService->save($follow);
        $testFollow    = $followService->getByPK($follow->getPostId(), $follow->getFollowerId());
        $this->assertEquals($follow, $testFollow);
        $this->assertEquals($testFollow->getPostId(), $post->getId());
        $this->assertEquals($testFollow->getFollowerId(), $user->getId());
    }

    public function testUpdateFollow()
    {
        $followService = Posts_Service_Follow::getInstance();
        $follow        = new Posts_Model_Follow();
        $user          = $this->createTestUser();
        $post          = $this->createTestPost($user);
        $follow->setPostId($post->getId());
        $follow->setFollowerId($user->getId());
        $follow->setNew(false);
        $follow        = $followService->save($follow);
    }

}

?>
